///MESHMAAMU
//CELL CONSTRUCTOR
//TAKES IN CELL ID, POINTER TO LINKED LIST OF NODES, IDS OF NEIGHBOURS
/////////////////////////////////////////////////////////////////////////////////
//CELL CLASS AND DATA STRUCTURE WITH NODES IN CELL AS DATA STRUCTURES
//CELL IS A LINKED LIST WHICH TERMINATES
//NODES ARE A RING
//NORMALS TERMINATE TO END
//REWRITE IT AS NORMALS BEING A RING TOO
//UNSTRUCTURED MESHER
/////////////////////////////////////////////////////////////////////////////////
//ROUTINES TO DELETE CELLS
//ROUTINES TO CUT CELLS
//ROUTINES TO INSERT CELLS INTO THE CELLS BETWEEN THE CURRENT CELL AND OLDER NEXT
//CELL ROUTINES CAN BE OVERLOADED FOR THE REGULAR CARTESIAN MESH AND KEPT
//GENERAL FOR THE CUT CELLS
//^OR WHY EVEN BOTHER, IT'S FAST ENOUGH AND SURELY THIS CFD CODE
//WILL NOT BE DIRECTLY USED IN AN OPTIMIZER LOOP.. SO DON'T WORRY..
//ITERATIONS AND FILE IO TAKE UP MOST OF THE TIME.
//ROUTINES TO MERGE CELLS IF SMALL
/////////////////////////////////////////////////////////////////////////////////
#ifndef _cell_h
#define _cell_h
#ifndef CFL
#define CFL 0.3
#endif
/////////////////////////////////////////////////////////////////////////////////
#include <cmath>
//#include <iostream>
/////////////////////////////////////////////////////////////////////////////////
#define float double
/////////////////////////////////////////////////////////////////////////////////
using namespace std;
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
//CLASS NODE IS
extern long double dx;
extern long double dy;
class node
{
public:
    node *next;                 //POINTER TO NEXT NODE IN LIST
    float x,y;                  //GLOBAL COORDINATES FOR NODE
    //int nodeid;                 //CELL RELATIVE NODE ID
    int gnodeid;                //NODE ID GLOBAL
    //1 IF INTERIOR 0 IF EXTERIOR
    int interior;
};
/////////////////////////////////////////////////////////////////////////////////
//FORWARD DECLARATION
#ifndef _circular_dependency
#define _circular_dependency
class cell;
class normal
{
public:
    //ASSOCIATES THE OUTWARD NORMAL FROM A CELL FACE TO THE CELL SHARING THE FACE
    cell *neighbour;
    //START NODE OF NORMAL
    node *snode,*lnode;         //DELETE THESE? MAYBE NOT
    //NEXT NORMAL IN THE LIST
    normal *next;               //POINTER TO NEXT NORMAL IN LIST
    float normalx,normaly;      //COMPONENTS OF NORMAL
    float area;                 //AREA OF FACE HAVING THE NORMAL
//    int normalid;               //CELL-RELATIVE NORMAL ID 0,1,2,3 AND SO ON
    //CELL ID OF THE NEIGHBOURING CELL SHARING THIS FACE
    //USED TO IDENTIFY BOUNDARIES -1 WALL, -2 SUP INFLOW AND SO ON
    int neighbourid;
    //CONVECTIVE FLUX VALUES OF ALL VARIABLES AT THIS FACE
    //THIS IS TO BE EVALUATED SOMEHOW BY ANY SORT OF SCHEME POSSIBLE/OPTIMAL
    float flux[4];
    bool if_flux_computed;
};
#endif
/////////////////////////////////////////////////////////////////////////////////
class cell
{
public:
    //IF ALL ITS NODES ARE INTERIOR THEN TRUE
    //IF ALL NODES ARE EXTERIOR THEN THE CELL IS EXTERIOR
    int interior;
    //CELL IS TO BE CUT IF AT LEAST ONE NODE IS INTERIOR AND ONE IS EXTERIOR
    //LINKS THE CURRENT CELL TO THE NEXT CELL IN THE LIST
    cell *next;                 //POINTER TO NEXT CELL
    //RIGHT NOW ONLY KEPT FOR PRINTING PURPOSES..
    //IF PRINTING COULD BE DONE IN AN EFFICIENT WAY, THEN ALL WOULD BE FINE..
    int cellid;                 //GLOBAL CELL ID
    int cellidstore;            //FOR THE RECONDITIONING    //DELETE THIS?
    //float cvolume;              //VOLUME OF CELL
    float cvolsigned;           //SIGNED VOLUME OF CELL
    float U[4],Utstep[4];       //SOLUTION RELATED VARIABLES
    node *fnode;                //POINTER TO FIRST NODE WRT CELL
    normal *fnormal;            //POINTER TO FIRST NORMAL IN THE CELL
    float dt;                   //LOCAL TIME STEP VALUE
    float scaled_density_residual;
    float a;                    //LOCAL SPEED OF SOUND
    float cx,cy;                //CENTROID COORDINATES
    bool mergestatus;
    float gradUx[4],gradUy[4];        //COMPONENTS OF GRADIENT OF U
    float Psi[4];                     //Slope Limiter
/////////////////////////////////////////////////////////////////////////////////
    //ALL THE FUNCTIONS BELOW ARE CALLED BY A CONSTRUCTOR
    //CONSTRUCTOR CALLS BELOW FUNCTIONS INCLUDING THE NODE GENERATOR
    //THE CONSTRUCTOR TAKES IN THE NUMBER OF NODES TO BE PUT INTO THIS CELL
    //IT ALSO TAKES IN THE FIRST NODE CO ORDINATES
    //MAKE NODE RING FUNCTION CALLS ANOTHER FUNCTION NOT INSIDE ALL THIS TO ASSIGN VALUES TO NODES
    //MAKE NODE RING
/////////////////////////////////////////////////////////////////////////////////
    //VOLUME FUNCITON
    float Vol()
    {
        node *tnode;            //TEMPORARY STEPPING NODE
        tnode=fnode;
        cvolsigned=0;
        do
        {
            cvolsigned+=0.5*((tnode->x*tnode->next->y)-(tnode->y*tnode->next->x));
            tnode=tnode->next;
        }while(tnode!=fnode);
        return cvolsigned;
    }
/////////////////////////////////////////////////////////////////////////////////
    //CENTROID FUNCTION
    node* Centroid()
    {
        node *centroid,*tnode;
        centroid=new node;
        tnode=fnode;
        float sigmax=0,sigmay=0;
        do
        {
            sigmax+=(tnode->x+tnode->next->x)*((tnode->x*tnode->next->y)-(tnode->next->x*tnode->y));
            sigmay+=(tnode->y+tnode->next->y)*((tnode->x*tnode->next->y)-(tnode->next->x*tnode->y));
            tnode=tnode->next;
        }while(tnode!=fnode);
        centroid->x=(1/(6*cvolsigned))*sigmax;
        centroid->y=(1/(6*cvolsigned))*sigmay;
        return centroid;
    }
/////////////////////////////////////////////////////////////////////////////////
    //USES THE SHOELACE FORMULA FOR AREA OF POLYGON
    float Area(node *tnode)
    {
        if(tnode->next->x!=tnode->x)
        {
            return sqrt(((tnode->next->x-tnode->x)*(tnode->next->x-tnode->x))+((tnode->next->y-tnode->y)*(tnode->next->y-tnode->y)));
        }
        else
        {
            return abs(tnode->next->y-tnode->y);
        }
    }
/////////////////////////////////////////////////////////////////////////////////
    //NORMALS GENERATED HERE
    //PROCESS IS RATHER LONG WINDED AND DIFFICULT TO EXPLAIN
    //BUT IT WORKS FOR ANY POLYGONAL CELL
    //OVERLOAD THIS FOR CARTESIAN CELLS TO AVOID ALL THIS COMPUTATION
    normal* Normal(node *tnode,int norid)
    {
        normal *rnormal;
        rnormal=new normal;
        float m,den,sgn;
        den=tnode->next->y-tnode->y;
        if(den>0)
        {
            sgn=1;
        }
        else
        {
            sgn=-1;
        }
        if(abs(den)!=0)
        {
            m=-(tnode->next->x-tnode->x)/(den);
            rnormal->normalx=sgn*1/sqrt(1+m*m);
            rnormal->normaly=sgn*m/sqrt(1+m*m);
            //rnormal->normalid=tnode->nodeid;
            rnormal->area=Area(tnode);
        }
        else
        {
            //rnormal->normalid=tnode->nodeid;
            rnormal->area=Area(tnode);
            rnormal->normalx=0;
            if(tnode->next->next->y>tnode->next->y)
            {
                rnormal->normaly=-1;
            }
            else
            {
                rnormal->normaly=1;
            }
        }
        //ASSOCIATES THE NORMAL TO A NEIGHBOUR
        rnormal->neighbourid=norid;
        rnormal->snode=tnode;
        rnormal->lnode=tnode->next;
        return rnormal;
    }
/////////////////////////////////////////////////////////////////////////////////
    //TIME STEP CALCULATOR
    ///TIME STEP CALCULATOR SHOULD USE LARGEST EDGE LENGTH
    float dtcal()
    {
        a=sqrt((gamma*(gamma-1))*((U[3]/U[0])-0.5*(U[1]*U[1]+U[2]*U[2])/(U[0]*U[0])));
        float Umax=max(U[1]/U[0]+a,U[2]/U[0]+a);
        Umax=max(Umax,U[1]/U[0]-a);
        Umax=max(Umax,U[2]/U[0]-a);
//        dt=CFL*(cvolume/max(dx,dy))/Umax;
//        return dt;
        normal *temp=fnormal;
        float max_area=fnormal->area;
        while(temp!=NULL)
        {
            if(temp->area>max_area)
            {
                max_area=temp->area;
            }
            temp=temp->next;
        }
        dt=CFL*(cvolsigned/max_area)/Umax;
        return dt;
    }
/////////////////////////////////////////////////////////////////////////////////
    //CELL CONSTRUCTOR
    //TAKES IN CELL ID, LINKED LIST OF NODES, IDS OF NEIGHBOURS
    cell()
    {
        mergestatus=false;
        next=NULL;
    }
    cell(int cid,node *firstn,int *neighbours)
    {
        mergestatus=false;
        interior=10;
        if(neighbours!=NULL)
        {
            scaled_density_residual=10;
            for(int i=0;i<4;i++)
            {
                Utstep[i]=1;
                U[i]=1;
            }
            //ASSIGN CELLID
            cellid=cid;
            cellidstore=cellid;
            fnode=firstn;

            //CREATE NORMALS
            node *currentn;
            normal *currentnorm;
            currentn=fnode;

            //CREATE THE FIRST OF THE NORMAL IN THE LIST
            fnormal=Normal(currentn,neighbours[0]);
            //ASSOCIATE THE NORMAL WITH ITS AREA
            fnormal->area=Area(currentn);
            //CAP THE LIST
            fnormal->next=NULL;
            //CURRENT NORMAL POINTER TO THE FIRST NORMAL
            currentnorm=fnormal;

            currentn=currentn->next;
            //COUNTER TO SEND THE NEIGHBOUR IDS
            int i=1;
            while(currentn!=fnode)
            {
                //CREATE NORMAL AT THE LOCATION WHERE THE LHS IS POINTING
                currentnorm->next=Normal(currentn,neighbours[i]);
                //SHIFT THE CURRENT NODE POINTER TO THE NEXT NODE
                currentn=currentn->next;
                //SHIFT THE CURRENT NORMAL TO THE NEXT LOCATION
                currentnorm=currentnorm->next;
                //INCREASE I BY 1
                i++;
            }
            //CAP THE NEXT POINTER OF THIS NEW CURRENTN TO NULL
            currentnorm->next=NULL;
            //DEFINE VOLUME OF THE CELL
            cvolsigned=Vol();
            cvolsigned=abs(cvolsigned);
            cx=Centroid()->x;
            cy=Centroid()->y;
            scaled_density_residual=10;
        }
        else
        {
            scaled_density_residual=10;
            cvolsigned=1;
            for(int i=0;i<4;i++)
            {
                Utstep[i]=1;
                U[i]=1;
            }
        }
    }
    //END OF CONSTRUCTOR
/////////////////////////////////////////////////////////////////////////////////
};
/////////////////////////////////////////////////////////////////////////////////
cell* getcell(int,cell*);
node* getnode(int,node*);
/////////////////////////////////////////////////////////////////////////////////
//RETURNS POINTER TO CELL GIVEN A PARTICULAR CELL ID
//THE FOLLOWING TWO FUNCTIONS HAVE EXTREMELY BAD ACCESS TIMES IF YOU TRY AND ACCESS FROM
//THE FIRST CELL ALL THE TIME.
//START FROM A CLOSER POINT AND CONSISTENTLY CLOSE AND OP COUNT REDUCES TO A ZEROTH ORDER
cell* getcell(int cid,cell *dummyfirstcell)
{

    cell* dummyc;
    dummyc=dummyfirstcell;
    while(dummyc!=NULL&&dummyc->cellid!=cid)
    {
            dummyc=dummyc->next;
    }
    if(dummyc!=NULL)
        return dummyc;
    else
    {
        std::cout<<std::endl<<"Fatal error..!! Trying to access non existent cell"<<std::endl;
        return NULL;
    }
}
/////////////////////////////////////////////////////////////////////////////////
node* getnode(int nid,node *dummyfirstn)
{

    node* dummyn;
    dummyn=dummyfirstn;
    while(dummyn!=NULL&&dummyn->gnodeid!=nid)
    {
            dummyn=dummyn->next;
    }
    if(dummyn!=NULL)
        return dummyn;
    else
    {
        std::cout<<std::endl<<"Fatal error..!! Trying to access non existent node"<<std::endl;
        system("pause");
        return NULL;
    }
}
//USE TYPEDEF TO CLEAN UP LONG POINTER CHAINS
//typedef class cell->normal->cell bottom;
#endif // _cell_h
