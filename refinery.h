///MESHMAAMU
/////////////////////////////////////////////////////////////////////////////////
#ifndef _REFINERY_H
#define _REFINERY_H
/////////////////////////////////////////////////////////////////////////////////
#define CRIT_RATIO 1e-1
/////////////////////////////////////////////////////////////////////////////////
//SPLIT A CELL INTO 4 CELLS.
//UPDATE NEIGHBOURS AND THE LIST
//INSERT THE SPLIT CELLS IN THE SAME POSITION AT THE LIST
//CELL MERGING ROUTINES AS WELL TO COMBINE SMALL CELLS TOGETHER..
/////////////////////////////////////////////////////////////////////////////////
//QUADTREE TYPE IMPLEMENTATION/SEARCHES FOR GEOMETRY ADAPTIVE REFINEMENT AND MULTIGRID
//EXTEND CELL CLASS TO HAVE 4 CHILD POINTERS, CELLID IS MADE 0,
//CHILDREN ARE PUT INTO THE BACKBONE LIST WITH NEIGHBOUR POINTERS APPROPRIATELY
//NO CHILD POINTERS
/////////////////////////////////////////////////////////////////////////////////
void merge_smallcells_genbycut(cell *firstc);
void refine_cell_isotropic_one_level(cell *largecell);
void undo_refinement_one_level(cell *firstc);
void autopick_cellcount_min(int *body_no,node **bodynodes);
/////////////////////////////////////////////////////////////////////////////////
//CHECKS THE NEIGHBOURS OF THE CELLS FOR THEIR VOLUMES
//IF VOLUME LESS THAN SOME CRITICAL RATIO, MERGE WITH THE NEIGHBOUR CELL FOUND
//UPDATE NORMAL LIST OF THE NEWLY FORMED SUPERCELL
//UPDATE NEIGHBOURS POINTERS OF ALL FORMER NEIGHBOURS
void merge_smallcells_genbycut(cell *firstc)
{
    cell *currentc=firstc;
    normal *tnormal;
    bool flag=false;
    while(currentc!=NULL)
    {
        ///MERGE ONLY IF CUT CELL
        if(currentc->interior==-1&&currentc->cellid!=0&&currentc->mergestatus==false)
        {
            tnormal=currentc->fnormal;
            while(tnormal!=NULL&&tnormal->neighbourid!=-666)
            {
                if(tnormal->neighbour!=NULL)
                {
                    ///IDEALLY, SEARCH FOR THE NEXT SMALLEST NEIGHBOUR
                    ///NEIGHBOUR SHOULD NOT BE INSIDE THE BODY
                    if((((float)(currentc->cvolume)/((float)(tnormal->neighbour->cvolume))))<=CRIT_RATIO&&tnormal->neighbour->cellid!=0&&tnormal->neighbour->interior==-1)
                    {
                        cout<<endl<<currentc->cellidstore<<"\tMerging small cell..!!!\t"<<currentc->cvolsigned<<"\t"<<tnormal->neighbour->cellid;
                        cout<<endl<<tnormal->neighbour->cvolsigned<<"\t"<<currentc->cvolsigned;
                        ///PERFORM THE MERGING HERE...!!!!!
                        ///GET THE LAST NORMAL OF THE BIG CELL
                        normal *lastnormal_bigcell=tnormal->neighbour->fnormal;
                        node *lastnode_bigcell=tnormal->neighbour->fnode;
                        normal *tempor=tnormal->neighbour->fnormal;
                        ///SET THE NORMAL WHICH IS MERGED UP TO THE DEVIL
                        tnormal->neighbourid=-666;
                        do
                        {
                            ///////////////////////////////////////////////////////////////
                            ///FIND THE NORMAL WHICH IS SHARED BETWEEN THE FACES AND OFFER IT'S NEIGHBOURID TO THE DEVIL
                            if(abs(tempor->normalx+tnormal->normalx)<1e-8&&abs(tempor->normaly+tnormal->normaly)<1e-8)
                            {
//                                cout<<endl<<"Coming here\n";
                                tempor->neighbour=tnormal->neighbour;
                                tempor->neighbourid=-666;
                            }
                            tempor=tempor->next;
                            ///////////////////////////////////////////////////////////////
                            lastnode_bigcell=lastnode_bigcell->next;
                            lastnormal_bigcell=lastnormal_bigcell->next;
                        }while(lastnormal_bigcell->next!=NULL);
//                        cout<<endl<<"Exiting loop\t";
                        ///SET CURRENTC->CELLID=0 AND UPDATE THE LIST OF THE BIGCELL AND ALSO THE VOL OF THE BIG CELL..
                        currentc->cellid=0;
                        currentc->interior=1;
                        tnormal->neighbour->cx=(tnormal->neighbour->cvolume*tnormal->neighbour->cx+currentc->cvolume*currentc->cx);
                        tnormal->neighbour->cy=(tnormal->neighbour->cvolume*tnormal->neighbour->cy+currentc->cvolume*currentc->cy);
                        tnormal->neighbour->cx/=(tnormal->neighbour->cvolume+currentc->cvolume);
                        tnormal->neighbour->cy/=(tnormal->neighbour->cvolume+currentc->cvolume);
                        tnormal->neighbour->cvolsigned+=currentc->cvolsigned;
                        tnormal->neighbour->cvolume=abs(tnormal->neighbour->cvolsigned);
                        lastnormal_bigcell->next=currentc->fnormal;
                        tnormal->neighbour->dt=tnormal->neighbour->dtcal();
                        currentc->mergestatus=true;
                        tnormal->neighbour->mergestatus=true;
                        ///FLUXES FROM THE SHARED FACE GO IN AND OUT AND CANCEL EACH OTHER OUT IF THE NUMERICAL
                        ///OTHERWISE, REMOVE THE SHARED FACE OR SET THE AREA OF THE SHARED FACE TO ZERO
                        ///FIND A WAY TO PUT IN THE NEW NODE LIST FOR CENTROID COMPUTATIONS...
                        ///THE NODE LIST MUST BE CORRECTED...
                        ///restart from the beginning
                        ///NEIGHBOURS OF THE CURRENTCELL HAVE TO BE UPDATED.
                        ///tnormal->neighbour is the BIG CELL
                        ///SET ALL NEIGHBOURS WHICH POINT TO CURRENTCELL TO POINT TO BIG CELL

                        cell *smallcell=currentc;
                        normal *normal_smallcell=smallcell->fnormal;
                        while(normal_smallcell!=NULL)
                        {
                            if(normal_smallcell->neighbour)
                            {
                                cell *neighbourcell=normal_smallcell->neighbour;
                                if(neighbourcell)
                                {
                                    normal *neighbour_normals=neighbourcell->fnormal;
                                    while(neighbour_normals!=NULL)
                                    {
                                        if(neighbour_normals->neighbour==currentc)
                                        {
                                            neighbour_normals->neighbour=tnormal->neighbour;
                                        }
                                        neighbour_normals=neighbour_normals->next;
                                    }
                                }
                            }
                            normal_smallcell=normal_smallcell->next;
                        }


                        tempor=tnormal->neighbour->fnormal;
//                        int i=0;
                        while(tempor!=NULL)
                        {
//                            i++;
                            //cout<<endl<<tempor->neighbourid;
                            tempor=tempor->next;
                        }
//                        cout<<"\n"<<i<<"\n"<<endl;
                        flag=true;
                        break;
                    }
                }
                tnormal=tnormal->next;
            }
        }
        else
        {
            currentc->dt=currentc->dtcal();
        }
        currentc=currentc->next;
        if(flag==true)
        {
//            cout<<"\nFlagged in merger\n\n";
            currentc=firstc;
            flag=false;
        }
    }
    cout<<"\nFinished merging"<<endl;
}
/////////////////////////////////////////////////////////////////////////////////
//END OF refinery.h HEADER
#endif // _REFINERY_H
/////////////////////////////////////////////////////////////////////////////////
