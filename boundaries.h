///MESHMAAMU
#ifndef _boundaries_h
#define _boundaries_h
/////////////////////////////////////////////////////////////////////////////////
#ifndef _inlet_conditions
#define _inlet_conditions
#define P_inlet 101325.0
#define U_inlet 347.188709493843*3
#define V_inlet 0.0
#define T_inlet 300
#define P_outlet 101325.0*0.001
#define R 287.0
#define gamma 1.4
#define Cv 717.5
#endif //_inlet_conditions
/////////////////////////////////////////////////////////////////////////////////
void inviscidwallboundary(cell*,normal*,float*);
void superinflowboundary(float*,float,float);
void superoutflowboundary(cell*,normal*,float*);
void subsonicoutflowboundary(cell*,normal*,float*);
/////////////////////////////////////////////////////////////////////////////////
void inviscidwallboundary(cell *leftcell,normal *currentface,float *Ur)
{
    Ur[0]=leftcell->U[0];
    if(currentface->normalx==0)
    {
        Ur[1]=+leftcell->U[1];
        Ur[2]=-leftcell->U[2];
    }
    //THIS ELSE CLAUSE WORKS IF normaly OF THE NORMAL IS ZERO
    else if(currentface->normaly==0)
    {
        Ur[1]=-leftcell->U[1];
        Ur[2]=+leftcell->U[2];
    }
    else
    {
        float ul=leftcell->U[1]/leftcell->U[0];
        float vl=leftcell->U[2]/leftcell->U[0];
        float nx=currentface->normalx;
        float ny=currentface->normaly;
        float ur,vr;
        ur=(ul*(ny*ny-nx*nx)-2*vl*nx*ny)/(nx*nx+ny*ny);
        vr=(vl*(nx*nx-ny*ny)-2*ul*nx*ny)/(nx*nx+ny*ny);
        Ur[1]=ur*Ur[0];
        Ur[2]=vr*Ur[0];
        //cout<<endl<<ul<<"\t"<<vl<<"\t\t"<<ur<<"\t"<<vr;
    }
    //VELOCITY NORMAL TO THE WALL IS REFLECTED.
    //VELOCITY PARALLEL TO THE WALL IS KEPT SAME..
    Ur[3]=leftcell->U[3];
}
/////////////////////////////////////////////////////////////////////////////////
//SUPER GENERAL.. NO RESTRICTIONS
void superinflowboundary(float *Ur,float x,float y)
{
    //P, R, T,Cv, V_inlet, U_inlet ARE TO BE GLOBAL VARIABLES OR DEFINED CONSTANTS
//    if(y>(float)0.35&&y<0.65)
//    {
////        cout<<"\nComing here..!!";
//        float u=2*sqrt(gamma*R*5*T_inlet);
//        Ur[0]=3*P_inlet/(R*5*T_inlet);
//        Ur[1]=Ur[0]*u/sqrt(2.0);
//        Ur[2]=Ur[0]*u/sqrt(2.0);
//        Ur[3]=Ur[0]*(Cv*5*T_inlet+0.5*(1*u*u+V_inlet*V_inlet));
//    }
//    else if(y>(float)0.0&&y<0.15)
//    {
////        cout<<"\nComing here..!!";
//        float u=2*sqrt(gamma*R*5*T_inlet);
//        Ur[0]=3*P_inlet/(R*5*T_inlet);
//        Ur[1]=1*Ur[0]*u;
//        Ur[2]=Ur[0]*V_inlet;
//        Ur[3]=Ur[0]*(Cv*5*T_inlet+0.5*(1*u*u+V_inlet*V_inlet));
//    }
//    else
    //{
        Ur[0]=1*P_inlet/(R*T_inlet);
        Ur[1]=1*Ur[0]*U_inlet;
        Ur[2]=Ur[0]*V_inlet;
        Ur[3]=Ur[0]*(Cv*T_inlet+0.5*(1*U_inlet*U_inlet+V_inlet*V_inlet));
        //x=1;y=1;
    //}
}
/////////////////////////////////////////////////////////////////////////////////
//WORKS ONLY IF 2 LAYERS OF QUAD CELLS ARE AT THE BOUNDARY IN QUESTION
//OTHERWISE NO RESTRICTIONS ON THE ORIENTATION OF THE CELLS OR THE RELATIVE CELL DIMENSIONS
//MODIFY THIS TO HANDLE SOME MORE GENERAL CASES LIKE CELLS NOT 4 SIDED
void superoutflowboundary(cell *leftcell,normal *rightcellface,float *Ur)
{
//
    //TEMPORARILY BRIDGE THE NORMALS INTO A LOOP SO THAT
    //BOUNDARIES AT ANY LOCATION TOP, BOTTOM, LEFT, RIGHT CAN BE HANDLED
    //THEN BREAK THE LOOP TO ALLOW FOR THE PREVIOUS IMPLEMENTATION OF
    //RESIDUAL FUNCTION TO SUM THROUGH THE FACES
//    normal *temporary=rightcellface;
//    //ITERATE TO THE LAST FACE, THEN BRIDGE IT TO THE FIRST FACE
//    do
//    //while(temporary->next!=NULL)
//    {
//        temporary=temporary->next;
//    }while(temporary->next!=NULL);
//    temporary->next=leftcell->fnormal;
//    //FOR QUAD CELL, PREVIOUS CELL IS AT THE OPPOSITE FACE OF THE OUTFLOW FACE
//    //OPPOSITE FACE CAN BE FOUND 2 FACES AWAY IN LINKED LIST
//    cell *previous=rightcellface->next->next->neighbour;
//    Ur[0]=2*leftcell->U[0]-1*previous->U[0];
//    Ur[1]=2*leftcell->U[1]-1*previous->U[1];
//    Ur[2]=2*leftcell->U[2]-1*previous->U[2];
//    Ur[3]=2*leftcell->U[3]-1*previous->U[3];
//    //BREAK THE LOOP BRIDGE TO RESET NORMAL LINKED LIST TO A LINEAR LIST.
//    temporary->next=NULL;
/////////////////////////////////////////////////////////////////////////////////
    ///FOR CELL WITH ARBITRARY SHAPE
/*
    cell *previous=NULL;
    normal *tempnormal;
    node center1,center2;
    float dist_sq,maxdistance=-1;
    tempnormal=leftcell->fnormal;
    center1.x=0.5*(rightcellface->snode->x+rightcellface->lnode->x);
    center1.y=0.5*(rightcellface->snode->y+rightcellface->lnode->y);
    while(tempnormal!=NULL)
    {
        if(tempnormal!=rightcellface)
        {
            center2.x=0.5*(tempnormal->snode->x+tempnormal->lnode->x);
            center2.y=0.5*(tempnormal->snode->y+tempnormal->lnode->y);
            dist_sq=(center1.x-center2.x)*(center1.x-center2.x)+(center1.y-center2.y)*(center1.y-center2.y);
            if(dist_sq>maxdistance)
            {
                maxdistance=dist_sq;
                if(tempnormal->neighbourid>0&&tempnormal->neighbour!=NULL)
                {
                    previous=tempnormal->neighbour;
                }
                else
                {
                    previous=leftcell;
                }
            }
        }
        tempnormal=tempnormal->next;
    }
    //FROM THE ABOVE, THE PREVIOUS FACE HAS BEEN FOUND OUT..
    //NOW DO THE SAME AS BEFORE
    Ur[0]=2*leftcell->U[0]-1*previous->U[0];
    Ur[1]=2*leftcell->U[1]-1*previous->U[1];
    Ur[2]=2*leftcell->U[2]-1*previous->U[2];
    Ur[3]=2*leftcell->U[3]-1*previous->U[3];
//    float T,mach;
   // T=(1/Cv)*((leftcell->U[3]/leftcell->U[0])-0.5*(leftcell->U[1]*leftcell->U[1]+leftcell->U[2]*leftcell->U[2])/(leftcell->U[0]*leftcell->U[0]));
*/
    subsonicoutflowboundary(leftcell,rightcellface,Ur);
}
void subsonicoutflowboundary(cell *leftcell,normal *rightcellface,float *Ur)
{
    //EXTRAPOLATE OTHER FLOW QUANTITIES. EXCEPT FOR PRESSURE
    //PRESSURE IS SPECIFIED
    //EXTRAPOLATE TEMPERATURE
    //EXTRAPOLATE VELOCITIES
    cell *previous=NULL;
    normal *tempnormal;
    node center1,center2;
    float dist_sq,maxdistance=-1;
    tempnormal=leftcell->fnormal;
    center1.x=0.5*(rightcellface->snode->x+rightcellface->lnode->x);
    center1.y=0.5*(rightcellface->snode->y+rightcellface->lnode->y);
    while(tempnormal!=NULL)
    {
        if(tempnormal!=rightcellface)
        {
            center2.x=0.5*(tempnormal->snode->x+tempnormal->lnode->x);
            center2.y=0.5*(tempnormal->snode->y+tempnormal->lnode->y);
            dist_sq=(center1.x-center2.x)*(center1.x-center2.x)+(center1.y-center2.y)*(center1.y-center2.y);
            if(dist_sq>maxdistance)
            {
                maxdistance=dist_sq;
                if(tempnormal->neighbourid>0&&tempnormal->neighbour!=NULL)
                {
                    previous=tempnormal->neighbour;
                }
                else
                {
                    previous=leftcell;
                }
            }
        }
        tempnormal=tempnormal->next;
    }
    //FROM THE ABOVE, THE PREVIOUS FACE HAS BEEN FOUND OUT..
    //NOW DO THE SAME AS BEFORE
    float T,Tprev,Tnext;
    T=(1/Cv)*((leftcell->U[3]/leftcell->U[0])-0.5*(leftcell->U[1]*leftcell->U[1]+leftcell->U[2]*leftcell->U[2])/(leftcell->U[0]*leftcell->U[0]));
    Tprev=(1/Cv)*((previous->U[3]/previous->U[0])-0.5*(previous->U[1]*previous->U[1]+previous->U[2]*previous->U[2])/(previous->U[0]*previous->U[0]));
    Tnext=2*T-Tprev;
    Ur[0]=P_outlet/(R*Tnext);
    Ur[1]=2*leftcell->U[1]-1*previous->U[1];
    Ur[2]=2*leftcell->U[2]-1*previous->U[2];
    //Ur[2]=0;
    Ur[3]=Ur[0]*(Cv*Tnext+0.5*(1*Ur[1]*Ur[1]+Ur[2]*Ur[2])/(Ur[0]*Ur[0]));
}
/////////////////////////////////////////////////////////////////////////////////
#endif // _boundaries_h
/////////////////////////////////////////////////////////////////////////////////
