///MESHMAAMU
//ROUTINES TO ALLOW FOR CELLS WITH CONNECTIVITY LOSS TO BE HANDLED CORRECTLY
//CONNECTIVITY LOSS HAPPENS WHEN WE HAVE MULTI BLOCK MESHES
//ALSO WHEN WE HAVE SMALL CELLS MERGED WITH OTHER CELLS.
#ifndef _Solver_h
#define _Solver_h
/////////////////////////////////////////////////////////////////////////////////
#ifndef _TIMESTEP
#define _TIMESTEP 1E-6
#endif // _TIMESTEP
//#undef _TIMESTEP
/////////////////////////////////////////////////////////////////////////////////
#include <cmath>
const float alpha=3.0/16.0;
const float beta=1.0/8.0;
const float sigma=1;
const float Ku=0.75;
const float Kp=0.25;
const float omega=0;   //Approximate value of Machine Accuracy
const float K=10;      //VENKATAKRISHNAN Limiter needs this constant
/////////////////////////////////////////////////////////////////////////////////
//INCLUDES IMPLEMENTATION OF FLUX SPLITTING BY AUSM
//RK4 TIME STEPPER WHICH GIVES RESIDUAL GIVEN LEFT STATE
//RESIDUAL FUNCTION TAKES IN THE LEFT STATE AND PROVIDES RESIDUALS
//RK4 TIME STEPS
//RESIDUAL FUNCTION CALLS IN THE AUSM FUNCTION TO PROVIDE THE FLUX
//AUSM FUNCTION TAKES IN LEFT AND RIGHT STATES AND DOES AUSM
//AUSM_PLUS_UP FUNCTION DOES AUSM+UP SCHEME?? HOPE SO
/////////////////////////////////////////////////////////////////////////////////
void ode45_tstep(cell*);
void residual(cell*,float*);
void fluxAUSM(cell*,normal*);
void fluxAUSM_plus_up(cell*,normal*);
void rightstater(cell*,normal*,float*);
void rightstater_reconstructed(cell*,normal*,float*,char choice[2]={"1a"});
void leftstater_reconstructed(cell*,normal*,float*,char choice[2]={"1a"});
void gradientU(cell*,char choice[2]={"1a"});
void limiter_VKS(cell*);
/////////////////////////////////////////////////////////////////////////////////
#ifdef _SOLVER_FUNCTIONS
//#ifndef _SOLVER_FUNCTIONS
#define _SOLVER_FUNCTIONS
void ode45_tstep(cell *leftcell)
{
    //cout<<endl<<"Solving"<<leftcell->cellid;
    cell *copycell=new cell(leftcell->cellid,leftcell->fnode,NULL);
    copycell->fnode=leftcell->fnode;
    copycell->fnormal=leftcell->fnormal;
    float tempU[4],R1[4],R2[4],R3[4],R4[4];
    tempU[0]=leftcell->U[0];
    tempU[1]=leftcell->U[1];
    tempU[2]=leftcell->U[2];
    tempU[3]=leftcell->U[3];
    copycell->U[0]=tempU[0];
    copycell->U[1]=tempU[1];
    copycell->U[2]=tempU[2];
    copycell->U[3]=tempU[3];
    copycell->cvolsigned=leftcell->cvolsigned;
    copycell->cellid=leftcell->cellid;
    #ifndef _TIMESTEP
    float dt=leftcell->dtcal();
    #endif // _TIMESTEP
/////////////////////////////////////////////////////////////////////////////////
    //STAGE 1
    residual(copycell,R1);
    //PREP FOR STAGE 2
    #ifdef _TIMESTEP
    float dt=_TIMESTEP;
    #endif // _TIMESTEP
    for(int i=0;i<4;i++)
    {
//        copycell->U[i]=0;
        copycell->U[i]=tempU[i]+0.5*dt*R1[i];
    }
    //STAGE 2
    residual(copycell,R2);
    //PREP FOR STAGE 3
    for(int i=0;i<4;i++)
    {
//        copycell->U[i]=0;
        copycell->U[i]=tempU[i]+0.5*dt*R1[i];
    }
    //STAGE 3
    residual(copycell,R3);
    //PREP FOR STAGE 4
    for(int i=0;i<4;i++)
    {
//        copycell->U[i]=0;
        copycell->U[i]=tempU[i]+dt*R1[i];
    }
    //STAGE 4
    residual(copycell,R4);
    //PERFORM THE FINAL RUNGE KUTTA TIMESTEPPING
    //MODIFY leftcell HERE
    for(int i=0;i<4;i++)
    {
        //cout<<(dt/6)*(R1[i]+2*(R2[i]+R3[i])+R4[i])<<"\t";
        leftcell->Utstep[i]+=(dt/6)*(R1[i]+2*(R2[i]+R3[i])+R4[i]);
        /////////////////////////////////////////////////////////////////////////
        //UNCOMMENT FOR GAUSS SEIDEL LIKE ITERATIONS.
        //leftcell->U[i]=leftcell->Utstep[i];
        if(isnan(leftcell->Utstep[i]))
        {
            leftcell->Utstep[i]=leftcell->U[i];
        }
        //copycell->U[i]+=(dt/6)*(R1[i]+2*(R2[i]+R3[i])+R4[i]);
    }
    //IF NEGATIVE, THEN BEWARE, YOUR DENSITY IS GOING NEGATIVE.. TERMINATE SOLUTION AND REINITIALISE
    leftcell->scaled_density_residual=abs(leftcell->Utstep[0]-leftcell->U[0])/leftcell->U[0];
//    if(leftcell->scaled_density_residual>0)
//        cout<<endl<<leftcell->cellid<<"\t"<<leftcell->scaled_density_residual;
//DELETE THE TEMPORARY COPYCELL
//OR JUST DON'T BOTHER WITH THE OVERHEAD MAYBE?? LOCAL VARIABLE AUTOMATICALLY DESTROYED
//IF IT GOES OUT OF SCOPE I GUESS??
    delete copycell;
    //THIS COMPLETES STEPPING ONE CELL FORWARD IN TIME BY ONE TIME STEP
}
/////////////////////////////////////////////////////////////////////////////////
void residual(cell *leftcell,float *residues)
{
    normal *currentface=leftcell->fnormal;
    float areabyvol;
    //SET ALL THE RESIDUALS TO ZERO
    for(int i=0;i<4;i++)
    {
        residues[i]=0;
    }
    //LOOPING THROUGH ALL NORMALS IN DEFINED WITH LEFTC
    while(currentface!=NULL)
    {
        if(currentface->neighbourid==-666)
        {
            currentface=currentface->next;
            continue;
        }
        for(int i=0;i<4;i++)
        {
            currentface->flux[i]=0;
        }
        //CALL THE AUSM FUNCTION TO FIND THE FLUXES AND ASSIGN IT TO THE FLUX ARRAY IN CURRENTFACE
        //CURRENTFACE HAS A NEIGHBOUR ID ASSOCIATED WITH IT
//        fluxAUSM(leftcell,currentface);
        fluxAUSM_plus_up(leftcell,currentface);
        //UPDATE ALL THE RESIDUALS TILL ALL FACES ARE SUMMED UP
        areabyvol=(currentface->area)/leftcell->cvolsigned;
//        if(leftcell->cellid==1&&currentface->normalid==0)
//            cout<<"FLUX OF MASS"<<"\t"<<currentface->flux[0]<<"\t"<<endl;
        //IF NEIGHBOUR QUAD CELL EXISTS FOR THIS FACE, SET FLUX FOR THAT FACE
        //SET THE IF FLUX COMPUTED FLAG TO 1 FOR THE NEIGHBOURING CELL FACES
        //SET FLUX COMPUTED TO 0 FOR THIS FACE.
        currentface->if_flux_computed=0;
        for(int i=0;i<4;i++)
        {
            residues[i]+=(-currentface->flux[i]*areabyvol);
        }
        //MOVE TO THE NEXT FACE
        currentface=currentface->next;
    }
}
/////////////////////////////////////////////////////////////////////////////////
//TAKES IN LEFT STATE, CALLS A FUNCTION TO FIND OUT WHAT THE RIGHT STATE SHOULD BE,
//TAKES IN THOSE TWO STATES AND COMBINES THEM UP TO FIND THE FLUX THROUGH AUSM
void fluxAUSM(cell *leftcell,normal *currentface)
{
/////////////////////////////////////////////////////////////////////////////////
    float Ur[4]={0,0,0,0};
    //RIGHTSTATER FUNCTION GETS YOU THE RIGHT STATE BY TAKING THE NEIGHBOURING CELL VALUES
    rightstater(leftcell,currentface,Ur);
/////////////////////////////////////////////////////////////////////////////////
    const float nx=currentface->normalx;
    const float ny=currentface->normaly;
    const float Ul1=leftcell->U[1]/leftcell->U[0];
    const float Ul2=leftcell->U[2]/leftcell->U[0];
    const float Ur1=Ur[1]/Ur[0];
    const float Ur2=Ur[2]/Ur[0];
    const float Vl=nx*Ul1+ny*Ul2;
    const float Vr=+nx*Ur1+ny*Ur2;
/////////////////////////////////////////////////////////////////////////////////
    const float clsq=(gamma*(gamma-1)*((leftcell->U[3]/leftcell->U[0])-0.5*(Ul1*Ul1+Ul2*Ul2)));
    const float crsq=(gamma*(gamma-1)*((Ur[3]/Ur[0])-0.5*(Ur1*Ur1+Ur2*Ur2)));
    const float cl=sqrt(clsq);
    const float cr=sqrt(crsq);
    const float Tl=cl*cl/(gamma*R);
    const float Tr=cr*cr/(gamma*R);
    const float Pl=leftcell->U[0]*R*Tl;
    const float Pr=Ur[0]*R*Tr;
    const float Ml=Vl/cl;
    const float Mr=Vr/cr;
/////////////////////////////////////////////////////////////////////////////////
    float Mlplus,Mrminus,Mhalf;
    float Plplus,Prminus,Phalf;
/////////////////////////////////////////////////////////////////////////////////
    if(Ml>=1)
    {
        Mlplus=Ml;
    }
    else if(abs(Ml)<1)
    {
        Mlplus=0.25*(Ml+1)*(Ml+1);
    }
    else
    {
        Mlplus=0;
    }

    if(Mr>=1)
    {
        Mrminus=0;
    }
    else if(abs(Mr)<1)
    {
        Mrminus=-0.25*(Mr-1)*(Mr-1);
    }
    else
    {
        Mrminus=Mr;
    }
    Mhalf=Mlplus+Mrminus;
///////////////////////
    //SPLIT PRESSURE COMPUTATION
    if(Ml>1)
    {
        Plplus=Pl;
    }
    else if(abs(Ml)<1)
    {
        Plplus=0.25*Pl*(Ml+1)*(Ml+1)*(2-Ml);
    }
    else
    {
        Plplus=0;
    }
    if(Mr>=1)
    {
        Prminus=0;
    }
    else if(abs(Mr)<1)
    {
        Prminus=0.25*Pr*(Mr-1)*(Mr-1)*(2+Mr);
    }
    else
    {
        Prminus=Pr;
    }
    Phalf=Plplus+Prminus;
    //IF PRESSURE GOES NEGATIVE PUT A LIMITER???
    //PERFORM FLUX SPLITTING
    if(Mhalf>=0)
    {
        currentface->flux[0]=Mhalf*leftcell->U[0]*cl+0;
        currentface->flux[1]=Mhalf*leftcell->U[1]*cl+nx*Phalf;
        currentface->flux[2]=Mhalf*leftcell->U[2]*cl+ny*Phalf;
        currentface->flux[3]=Mhalf*(leftcell->U[3]+Pl)*cl;
    }
    else
    {
        currentface->flux[0]=Mhalf*Ur[0]*cr+0;
        currentface->flux[1]=Mhalf*Ur[1]*cr+nx*Phalf;
        currentface->flux[2]=Mhalf*Ur[2]*cr+ny*Phalf;
        currentface->flux[3]=Mhalf*(Ur[3]+Pr)*cr+0;
    }
    //IF ANY FLUX SPLITTING SCHEME OF THE ABOVE VARIABLE TEMPLATE DOESN'T WORK
    //THEN JUST USE THIS COMMENTED SECTION TO TEST.. COPY AND PASTE IT OVER THERE
    //IT'S A SIMPLE AVERAGE OF VARIABLES FLUX COMPUTOR
    //FLUX FROM AVERAGE OF VALUES AT FACES
//    float Uhalf[4];
//    float preshalf;
//    preshalf=0.5*(Pl+Pr);
//    for(int i=0;i<4;i++)
//    {
//        Uhalf[i]=0.5*(leftcell->U[0]+Ur[0]);
//    }
//    const float Vhalf=0.5*(Vl+Vr);
//    currentface->flux[0]=Uhalf[0]*Vhalf;
//    currentface->flux[1]=Uhalf[1]*Vhalf+nx*preshalf;
//    currentface->flux[2]=Uhalf[2]*Vhalf+ny*preshalf;
//    currentface->flux[3]=Vhalf*(Uhalf[3]+preshalf);

    //if(leftcell->cellid==1&&currentface->normalid==0)
            //cout<<"AUSM"<<"\t"<<currentface->neighbourid<<"\t"<<currentface->flux[]<<"\t"<<endl;

}
//END OF AUSM
/////////////////////////////////////////////////////////////////////////////////
//FLUX COMPUTATION BY AUSM_PLUS_UP
void fluxAUSM_plus_up(cell *leftcell,normal* currentface)
{
/////////////////////////////////////////////////////////////////////////////////
    float Ur[4]={0,0,0,0};
    //RIGHTSTATER FUNCTION GETS YOU THE RIGHT STATE BY TAKING THE NEIGHBOURING CELL VALUES
    rightstater(leftcell,currentface,Ur);
/////////////////////////////////////////////////////////////////////////////////
    //CONTRAVARIANT VELOCITIES AT THE LEFT AND RIGHT STATESs
    const float nx=currentface->normalx;
    const float ny=currentface->normaly;
    const float Ul1=leftcell->U[1]/leftcell->U[0];
    const float Ul2=leftcell->U[2]/leftcell->U[0];
    const float Ur1=Ur[1]/Ur[0];
    const float Ur2=Ur[2]/Ur[0];
    const float Vl=nx*Ul1+ny*Ul2;
    const float Vr=+nx*Ur1+ny*Ur2;
    //FOR AUSM + UP WE NEED a1/2 THAT REQUIRES AL CAP AND AR CAP
    //FOR THAT WE NEED A_STAR.
    //FOR THAT WE NEED TOTAL ENTHALPY AT THE CELL
    //FOR THAT PRESSURE AT THE CELL
    const float clsq=(gamma*(gamma-1)*((leftcell->U[3]/leftcell->U[0])-0.5*(Ul1*Ul1+Ul2*Ul2)));
    const float Tl=clsq/(gamma*R);
    const float crsq=(gamma*(gamma-1)*((Ur[3]/Ur[0])-0.5*(Ur1*Ur1+Ur2*Ur2)));
    const float Tr=crsq/(gamma*R);
    const float Pl=leftcell->U[0]*R*Tl;
    const float Pr=Ur[0]*R*Tr;
    const float Ht=(leftcell->U[3]+Pl)/leftcell->U[0];    //TOTAL ENTHALPY OF CELL.. NEEDED FOR CRITICAL SOUND SPEED
    const float a_star=sqrt(2*(gamma-1)*Ht/(gamma+1));
    const float al_hat=a_star*a_star/(max(a_star,Vl));
    const float ar_hat=a_star*a_star/(max(a_star,-Vr));
    const float a_half=min(al_hat,ar_hat);              //INTERFACE SPEED OF SOUND WITH DIRECTIONAL INFO
    const float Ml=Vl/a_half;
    const float Mr=Vr/a_half;
    const float M_bar_sq=(0.5*((Ml*Ml)+(Mr*Mr)));
    float Mlplus4,Mrminus4,Plplus5,Prminus5;
    float inside;
    if(abs(Ml)>=1)
    {
        Mlplus4=0.5*(Ml+abs(Ml));
    }
    else
    {
        inside=1-16*beta*(-0.25)*(Ml-1)*(Ml-1);
        Mlplus4=0.25*(Ml+1)*(Ml+1)*inside;
    }
    if(abs(Mr)>=1)
    {
        Mrminus4=0.5*(Mr-abs(Mr));
    }
    else
    {
        inside=1+16*beta*(0.25)*(Mr+1)*(Mr+1);
        Mrminus4=-0.25*(Mr-1)*(Mr-1)*inside;
    }
    if(abs(Ml)>=1)
    {
        Plplus5=0.5*(Ml+abs(Ml))/Ml;
    }
    else
    {
        inside=(+2-Ml)-16*alpha*Ml*(-0.25*(Ml-1)*(Ml-1));
        Plplus5=0.25*(Ml+1)*(Ml+1)*inside;
    }
    if(abs(Mr)>=1)
    {
        Prminus5=0.5*(Mr-abs(Mr))/Mr;
    }
    else
    {
        inside=(-2-Mr)+16*alpha*Mr*(0.25*(Mr+1)*(Mr+1));
        Prminus5=-0.25*(Mr-1)*(Mr-1)*inside;
    }


    const float Mp=-Kp*max(1-sigma*M_bar_sq,(float)0.00)*(Pr-Pl)/(a_half*a_half*0.5*(leftcell->U[0]+Ur[0]));
    const float M_half=Mlplus4+Mrminus4+Mp;     //INTERFACE MACH NUMBER
    const float Pu=-Ku*Plplus5*Prminus5*(leftcell->U[0]+Ur[0])*a_half*(Vr-Vl);
    const float P_half=Plplus5*Pl+Prminus5*Pr+Pu;   //INTERFACE SPLIT PRESSURE
/////////////////////////////////////////////////////////////////////////////////
    //NOW WITH ALL THOSE HUMONGOUS NUMBER OF VARIABLES ABOVE DO THE AUSM
    //IS THERE ANY WAY OF REDUCING ALL THAT STUFF???
    if(M_half>=0)
    {
        currentface->flux[0]=M_half*leftcell->U[0]*a_half+0;
        currentface->flux[1]=M_half*leftcell->U[1]*a_half+nx*P_half;
        currentface->flux[2]=M_half*leftcell->U[2]*a_half+ny*P_half;
        currentface->flux[3]=M_half*(leftcell->U[3]+Pl)*a_half;
    }
    else
    {
        currentface->flux[0]=M_half*Ur[0]*a_half+0;
        currentface->flux[1]=M_half*Ur[1]*a_half+nx*P_half;
        currentface->flux[2]=M_half*Ur[2]*a_half+ny*P_half;
        currentface->flux[3]=M_half*(Ur[3]+Pr)*a_half+0;
    }
/////////////////////////////////////////////////////////////////////////////////
}
//END OF AUSM_PLUS_UP
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//GIVES US WHAT Ur SHOULD BE BY CALLING VARIOUS OTHER FUNCTIONS
//EITHER THIS OR WRITE THE FUNCTIONS EXPLICITLY HERE
void rightstater(cell *leftcell,normal *rightcellface,float *Ur)
{
    //cell *firstc=firstcell;
//    cout<<endl<<rightcellface->neighbourid;
    switch(rightcellface->neighbourid)
    {
    case -1:        //INVISCID WALL BOUNDARY
        {
            inviscidwallboundary(leftcell,rightcellface,Ur);
            break;
        }
    case -2:        //INFLOW BOUNDARY
        {
            superinflowboundary(Ur,0.5*(rightcellface->snode->x+rightcellface->lnode->x),0.5*(rightcellface->snode->y+rightcellface->lnode->y));
            break;
        }
    case -3:        //OUTFLOW BOUNDARY
        {
            superoutflowboundary(leftcell,rightcellface,Ur);
            break;
        }
        //DEFAULT IF NOT A BOUNDARY
    default:
        {
            //EXPENSIVE GETCELL CALL IS AVOIDED BY USING THE NEIGHBOUR POINTER
            //cell *tempor=getcell(rightcellface->neighbourid,firstc);
            if(rightcellface->neighbour!=NULL)
            {
                cell *tempor=rightcellface->neighbour;
                Ur[0]=tempor->U[0];
                Ur[1]=tempor->U[1];
                Ur[2]=tempor->U[2];
                Ur[3]=tempor->U[3];
            }
            else
            {
                cout<<endl<<rightcellface->next->neighbour->cellid<<"You screwed up..!"<<endl;
                exit(-5);
            }
        }
    }
}
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
#endif  //_SOLVER_FUNCTIONS
//#undef _SOLVER_FUNCTIONS //Undefining the first order solver functions. Comment this line to use them
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// DEFINING SOLVER FUNCTIONS WITH LINEAR RECONSTRUCTION HERE
#ifndef _SOLVER_FUNCTIONS
#define _SOLVER_FUNCTIONS
void ode45_tstep(cell *leftcell)
{
//if(leftcell->cellid==27730)
//cout<<endl<<"DEBUG : Solving"<<leftcell->cellid;

    cell *copycell=new cell(leftcell->cellid,leftcell->fnode,NULL);
    copycell->fnode=leftcell->fnode;
    copycell->fnormal=leftcell->fnormal;
    float tempU[4],R1[4],R2[4],R3[4],R4[4];
    tempU[0]=leftcell->U[0];
    tempU[1]=leftcell->U[1];
    tempU[2]=leftcell->U[2];
    tempU[3]=leftcell->U[3];
    copycell->U[0]=tempU[0];
    copycell->U[1]=tempU[1];
    copycell->U[2]=tempU[2];
    copycell->U[3]=tempU[3];
    copycell->cvolsigned=leftcell->cvolsigned;
    copycell->cellid=leftcell->cellid;
    #ifndef _TIMESTEP
    float dt=leftcell->dtcal();
    #endif // _TIMESTEP
/////////////////////////////////////////////////////////////////////////////////
    //STAGE 1
    residual(copycell,R1);

//if(leftcell->cellid==27730)
//cout<<endl<<"DEBUG : RESIDUAL 1 DONE"<<leftcell->cellid;

    //PREP FOR STAGE 2
    #ifdef _TIMESTEP
    float dt=_TIMESTEP;
    #endif // _TIMESTEP
    for(int i=0;i<4;i++)
    {
//        copycell->U[i]=0;
        copycell->U[i]=tempU[i]+0.5*dt*R1[i];
    }
    //STAGE 2
    residual(copycell,R2);
    //PREP FOR STAGE 3
    for(int i=0;i<4;i++)
    {
//        copycell->U[i]=0;
        copycell->U[i]=tempU[i]+0.5*dt*R1[i];
    }
    //STAGE 3
    residual(copycell,R3);
    //PREP FOR STAGE 4
    for(int i=0;i<4;i++)
    {
//        copycell->U[i]=0;
        copycell->U[i]=tempU[i]+dt*R1[i];
    }
    //STAGE 4
    residual(copycell,R4);
    //PERFORM THE FINAL RUNGE KUTTA TIMESTEPPING
    //MODIFY leftcell HERE
    for(int i=0;i<4;i++)
    {
        //cout<<(dt/6)*(R1[i]+2*(R2[i]+R3[i])+R4[i])<<"\t";
        leftcell->Utstep[i]+=(dt/6)*(R1[i]+2*(R2[i]+R3[i])+R4[i]);
        /////////////////////////////////////////////////////////////////////////
        //UNCOMMENT FOR GAUSS SEIDEL LIKE ITERATIONS.
        //leftcell->U[i]=leftcell->Utstep[i];
        if(isnan(leftcell->Utstep[i]))
        {
            leftcell->Utstep[i]=leftcell->U[i];
        }
        //copycell->U[i]+=(dt/6)*(R1[i]+2*(R2[i]+R3[i])+R4[i]);
    }
    //IF NEGATIVE, THEN BEWARE, YOUR DENSITY IS GOING NEGATIVE.. TERMINATE SOLUTION AND REINITIALISE
    leftcell->scaled_density_residual=abs(leftcell->Utstep[0]-leftcell->U[0])/leftcell->U[0];
//    if(leftcell->scaled_density_residual>0)
//        cout<<endl<<leftcell->cellid<<"\t"<<leftcell->scaled_density_residual;
//DELETE THE TEMPORARY COPYCELL
//OR JUST DON'T BOTHER WITH THE OVERHEAD MAYBE?? LOCAL VARIABLE AUTOMATICALLY DESTROYED
//IF IT GOES OUT OF SCOPE I GUESS??
    delete copycell;
    //THIS COMPLETES STEPPING ONE CELL FORWARD IN TIME BY ONE TIME STEP
//cout<<endl<<"DEBUG : END OF ODE45"<<leftcell->cellid;
}
/////////////////////////////////////////////////////////////////////////////////
void residual(cell *leftcell,float *residues)
{
//if(leftcell->cellid==27730)
//cout<<endl<<"DEBUG : INSIDE RESIDUAL FUNCTION"<<leftcell->cellid<<endl;

    normal *currentface=leftcell->fnormal;
    cell *rightcell;
    float areabyvol;
    //SET ALL THE RESIDUALS TO ZERO
    for(int i=0;i<4;i++)
    {
        residues[i]=0;
    }
    // EVALUATE THE GRADIENTS OF U AND ASSIGN TO CELL CLASS VARIABLES
    gradientU(leftcell);
//cout<<endl<<"DEBUG : BEFORE CALLING LIMITER FUNCTION"<<leftcell->cellid;
    limiter_VKS(leftcell);
//cout<<endl<<"DEBUG : AFTER LIMITER FUNCTION"<<leftcell->cellid;
//if(leftcell->cellid==27730)
//cout<<endl<<"DEBUG : AFTER LEFT CELL GRADIENT"<<leftcell->cellid;

    //LOOPING THROUGH ALL NORMALS IN DEFINED WITH LEFTC
    while(currentface!=NULL)
    {
//if(leftcell->cellid==27730)
//cout<<endl<<"DEBUG : INSIDE WHILE LOOP";
        // GRADIENT CALCULATION FOR THE RIGHT CELL
        if(currentface->neighbourid>0) // BOUNDARY CELLS DO NEED GRADIENT CALCULATION/OR NOT BEING DONE NOW
            rightcell=currentface->neighbour;
//if(leftcell->cellid==27730)
//cout<<endl<<"DEBUG : CURRENTFACE"<<currentface->neighbourid;
//if(leftcell->cellid==27730)
//cout<<endl<<"DEBUG : BEFORE RIGHT CELL GRADIENT"<<rightcell->cellid;
        if(currentface->neighbourid>0) // BOUNDARY CELLS DO NEED GRADIENT CALCULATION/OR NOT BEING DONE NOW
        {
          gradientU(rightcell);
//cout<<endl<<"DEBUG : BEFORE CALLING RIGHTCELL LIMITER FUNCTION"<<rightcell->cellid;
          limiter_VKS(rightcell);
//cout<<endl<<"DEBUG : AFTER CALLING RIGHTCELL LIMITER FUNCTION"<<rightcell->cellid;
        }

//if(leftcell->cellid==27730)
//cout<<endl<<"DEBUG : AFTER RIGHT CELL GRADIENT"<<rightcell->cellid;
        /////////////////////////////////////////////////////////////////////////////
        if(currentface->neighbourid==-666)
        {
            currentface=currentface->next;
            continue;
        }
        for(int i=0;i<4;i++)
        {
            currentface->flux[i]=0;
        }
        //CALL THE AUSM FUNCTION TO FIND THE FLUXES AND ASSIGN IT TO THE FLUX ARRAY IN CURRENTFACE
        //CURRENTFACE HAS A NEIGHBOUR ID ASSOCIATED WITH IT
//        fluxAUSM(leftcell,currentface);
//cout<<endl<<"DEBUG : BEFOR AUSM PLUS UP";
        fluxAUSM_plus_up(leftcell,currentface);
//cout<<endl<<"DEBUG : AFTER AUSM PLUS UP";
        //UPDATE ALL THE RESIDUALS TILL ALL FACES ARE SUMMED UP
        areabyvol=(currentface->area)/leftcell->cvolsigned;
//        if(leftcell->cellid==1&&currentface->normalid==0)
//            cout<<"FLUX OF MASS"<<"\t"<<currentface->flux[0]<<"\t"<<endl;
        //IF NEIGHBOUR QUAD CELL EXISTS FOR THIS FACE, SET FLUX FOR THAT FACE
        //SET THE IF FLUX COMPUTED FLAG TO 1 FOR THE NEIGHBOURING CELL FACES
        //SET FLUX COMPUTED TO 0 FOR THIS FACE.
        currentface->if_flux_computed=0;
        for(int i=0;i<4;i++)
        {
            residues[i]+=(-currentface->flux[i]*areabyvol);
        }
        //MOVE TO THE NEXT FACE
        currentface=currentface->next;
//if(leftcell->cellid==27730)
//cout<<endl<<"DEBUG : INCREMENTED"<<rightcell->cellid;
    }
}
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// GRADIENT OF U CALCULATION FUNCTION
// CALCULATES AND ASSIGNS VALUES TO CELL CLASS VARIABLES gradUx AND gradUy
// choice VARIABLE WILL BE USED TO SELECT THE PARTICULAR GRADIENT CALCULATION METHOD
//      1a - FIRST ORDER EVALUATION USING SIMPLE GREEN-GAUSS METHOD
void gradientU(cell *currentcell,char choice[2])
{
//cout<<endl<<"DEBUG : INSIDE GRADIENTU FUNCTION"<<currentcell->cellid;
  normal *currentface=currentcell->fnormal;
  float Ui[4],Uj[4],gradUx[4],gradUy[4],areabyvol;
  //Setting Gradient Values to Zero
  for(int i=0;i<4;i++)
  {
      gradUx[i]=0;
      gradUy[i]=0;
  }
  // Current Cell Values
  for(int iter=0;iter<4;iter++)
  {
    Ui[iter]=currentcell->U[iter];
  }
  // Looping through all the cell faces
  while(currentface!=NULL)
  {
      // NOT SURE WHAT THIS IF IS DOING.. COPIED FROM THE AUSM FUNCTION
      if(currentface->neighbourid==-666)
      {
          currentface=currentface->next;
          continue;
cout<<endl<<"DEBUG : INTO THE UNKNOWN IF CONDITION"<<currentcell->cellid;

      }

      areabyvol=(currentface->area)/currentcell->cvolsigned;
//cout<<endl<<"DEBUG : AREA BY VOL :"<<currentcell->cellid<<"   "<<areabyvol;

      // Right Cell Values
      rightstater(currentcell,currentface,Uj);

      for(int iter=0;iter<4;iter++)
      {
          gradUx[iter]+=0.5*(Ui[iter]+Uj[iter])*currentface->normalx*areabyvol;
          gradUy[iter]+=0.5*(Ui[iter]+Uj[iter])*currentface->normaly*areabyvol;
//cout<<endl<<"DEBUG : GRADIENT VALUES FOR CELL"<<currentcell->cellid<<" : "<<gradUx[iter]<<","<<gradUy[iter];
      }

      //MOVE TO THE NEXT FACE
      currentface=currentface->next;
//cout<<endl<<"DEBUG : AFTER INCREMENTING";
  }
  for(int i=0;i<4;i++)
  {
    currentcell->gradUx[i]=gradUx[i];
    currentcell->gradUy[i]=gradUy[i];
//cout<<endl<<"DEBUG : GRADIENT VALUES FOR CELL"<<currentcell->cellid<<" : "<<gradUx[i]<<","<<gradUy[i];
  }

} // END OF gradientU FUNCTION
///////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//AUSM_PLUS_UP WITH RECONSTRUCTION
void fluxAUSM_plus_up(cell *leftcell,normal* currentface)
{
    float Ul[4]={0,0,0,0};
//cout<<endl<<"DEBUG : BEFORE LEFTSTATER";
    //leftstater_reconstructed FUNCTION GETS THE LEFT FACE VALUES BASED ON A RECONSTRUCTION
    leftstater_reconstructed(leftcell,currentface,Ul);
//cout<<endl<<"DEBUG : AFTER LEFTSTATER";
/////////////////////////////////////////////////////////////////////////////////
    float Ur[4]={0,0,0,0};
    //rightstater_reconstructed FUNCTION GETS THE RIGHT STATE BASED ON A RECONSTRUCTION
    rightstater_reconstructed(leftcell,currentface,Ur);
/////////////////////////////////////////////////////////////////////////////////
    //CONTRAVARIANT VELOCITIES AT THE LEFT AND RIGHT STATESs
    const float nx=currentface->normalx;
    const float ny=currentface->normaly;
    const float Ul1=Ul[1]/Ul[0];
    const float Ul2=Ul[2]/Ul[0];
    const float Ur1=Ur[1]/Ur[0];
    const float Ur2=Ur[2]/Ur[0];
    const float Vl=nx*Ul1+ny*Ul2;
    const float Vr=+nx*Ur1+ny*Ur2;
    //FOR AUSM + UP WE NEED a1/2 THAT REQUIRES AL CAP AND AR CAP
    //FOR THAT WE NEED A_STAR.
    //FOR THAT WE NEED TOTAL ENTHALPY AT THE CELL
    //FOR THAT PRESSURE AT THE CELL
    const float clsq=(gamma*(gamma-1)*((leftcell->U[3]/leftcell->U[0])-0.5*(Ul1*Ul1+Ul2*Ul2)));
    const float Tl=clsq/(gamma*R);
    const float crsq=(gamma*(gamma-1)*((Ur[3]/Ur[0])-0.5*(Ur1*Ur1+Ur2*Ur2)));
    const float Tr=crsq/(gamma*R);
    const float Pl=leftcell->U[0]*R*Tl;
    const float Pr=Ur[0]*R*Tr;
    const float Ht=(leftcell->U[3]+Pl)/leftcell->U[0];    //TOTAL ENTHALPY OF CELL.. NEEDED FOR CRITICAL SOUND SPEED
    const float a_star=sqrt(2*(gamma-1)*Ht/(gamma+1));
    const float al_hat=a_star*a_star/(max(a_star,Vl));
    const float ar_hat=a_star*a_star/(max(a_star,-Vr));
    const float a_half=min(al_hat,ar_hat);              //INTERFACE SPEED OF SOUND WITH DIRECTIONAL INFO
    const float Ml=Vl/a_half;
    const float Mr=Vr/a_half;
    const float M_bar_sq=(0.5*((Ml*Ml)+(Mr*Mr)));
    float Mlplus4,Mrminus4,Plplus5,Prminus5;
    float inside;
    if(abs(Ml)>=1)
    {
        Mlplus4=0.5*(Ml+abs(Ml));
    }
    else
    {
        inside=1-16*beta*(-0.25)*(Ml-1)*(Ml-1);
        Mlplus4=0.25*(Ml+1)*(Ml+1)*inside;
    }
    if(abs(Mr)>=1)
    {
        Mrminus4=0.5*(Mr-abs(Mr));
    }
    else
    {
        inside=1+16*beta*(0.25)*(Mr+1)*(Mr+1);
        Mrminus4=-0.25*(Mr-1)*(Mr-1)*inside;
    }
    if(abs(Ml)>=1)
    {
        Plplus5=0.5*(Ml+abs(Ml))/Ml;
    }
    else
    {
        inside=(+2-Ml)-16*alpha*Ml*(-0.25*(Ml-1)*(Ml-1));
        Plplus5=0.25*(Ml+1)*(Ml+1)*inside;
    }
    if(abs(Mr)>=1)
    {
        Prminus5=0.5*(Mr-abs(Mr))/Mr;
    }
    else
    {
        inside=(-2-Mr)+16*alpha*Mr*(0.25*(Mr+1)*(Mr+1));
        Prminus5=-0.25*(Mr-1)*(Mr-1)*inside;
    }


    const float Mp=-Kp*max(1-sigma*M_bar_sq,(float)0.00)*(Pr-Pl)/(a_half*a_half*0.5*(leftcell->U[0]+Ur[0]));
    const float M_half=Mlplus4+Mrminus4+Mp;     //INTERFACE MACH NUMBER
    const float Pu=-Ku*Plplus5*Prminus5*(leftcell->U[0]+Ur[0])*a_half*(Vr-Vl);
    const float P_half=Plplus5*Pl+Prminus5*Pr+Pu;   //INTERFACE SPLIT PRESSURE
/////////////////////////////////////////////////////////////////////////////////
    //NOW WITH ALL THOSE HUMONGOUS NUMBER OF VARIABLES ABOVE DO THE AUSM
    //IS THERE ANY WAY OF REDUCING ALL THAT STUFF???
    if(M_half>=0)
    {
        currentface->flux[0]=M_half*leftcell->U[0]*a_half+0;
        currentface->flux[1]=M_half*leftcell->U[1]*a_half+nx*P_half;
        currentface->flux[2]=M_half*leftcell->U[2]*a_half+ny*P_half;
        currentface->flux[3]=M_half*(leftcell->U[3]+Pl)*a_half;
    }
    else
    {
        currentface->flux[0]=M_half*Ur[0]*a_half+0;
        currentface->flux[1]=M_half*Ur[1]*a_half+nx*P_half;
        currentface->flux[2]=M_half*Ur[2]*a_half+ny*P_half;
        currentface->flux[3]=M_half*(Ur[3]+Pr)*a_half+0;
    }
/////////////////////////////////////////////////////////////////////////////////
}
//END OF AUSM_PLUS_UP
////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//GIVES US Ur BY CALLING VARIOUS OTHER FUNCTIONS
void rightstater(cell *leftcell,normal *rightcellface,float *Ur)
{
    //cell *firstc=firstcell;
//    cout<<endl<<rightcellface->neighbourid;
    switch(rightcellface->neighbourid)
    {
    case -1:        //INVISCID WALL BOUNDARY
        {
            inviscidwallboundary(leftcell,rightcellface,Ur);
            break;
        }
    case -2:        //INFLOW BOUNDARY
        {
            superinflowboundary(Ur,0.5*(rightcellface->snode->x+rightcellface->lnode->x),0.5*(rightcellface->snode->y+rightcellface->lnode->y));
            break;
        }
    case -3:        //OUTFLOW BOUNDARY
        {
            superoutflowboundary(leftcell,rightcellface,Ur);
            break;
        }
        //DEFAULT IF NOT A BOUNDARY
    default:
        {
            //EXPENSIVE GETCELL CALL IS AVOIDED BY USING THE NEIGHBOUR POINTER
            //cell *tempor=getcell(rightcellface->neighbourid,firstc);
            if(rightcellface->neighbour!=NULL)
            {
                cell *tempor=rightcellface->neighbour;
                Ur[0]=tempor->U[0];
                Ur[1]=tempor->U[1];
                Ur[2]=tempor->U[2];
                Ur[3]=tempor->U[3];
            }
            else
            {
                cout<<endl<<rightcellface->next->neighbour->cellid<<"You screwed up..!"<<endl;
                exit(-5);
            }
        }
    }
}
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
//GIVES US Ur BY CALLING VARIOUS OTHER FUNCTIONS AND THEN RECONSTRUCTING
//choice VARIABLE IS THERE FOR SELECTION OF METHOD OF RECONSTRUCTION
//      1a- LINEAR RECONSTRUCTION BASED ON [BARTH,1989]
void rightstater_reconstructed(cell *leftcell,normal *rightcellface,float *Ur,char choice[2])
{
    //cell *firstc=firstcell;
//    cout<<endl<<rightcellface->neighbourid;
    switch(rightcellface->neighbourid)
    {
    case -1:        //INVISCID WALL BOUNDARY
        {
            inviscidwallboundary(leftcell,rightcellface,Ur);
            break;
        }
    case -2:        //INFLOW BOUNDARY
        {
            superinflowboundary(Ur,0.5*(rightcellface->snode->x+rightcellface->lnode->x),0.5*(rightcellface->snode->y+rightcellface->lnode->y));
            break;
        }
    case -3:        //OUTFLOW BOUNDARY
        {
            superoutflowboundary(leftcell,rightcellface,Ur);
            break;
        }
        //DEFAULT IF NOT A BOUNDARY
    default:
        {
            //EXPENSIVE GETCELL CALL IS AVOIDED BY USING THE NEIGHBOUR POINTER
            //cell *tempor=getcell(rightcellface->neighbourid,firstc);
            if(rightcellface->neighbour!=NULL)
            {
                // FOR NON-BOUNDARY FACES, RECONSTRUCTED SOLUTION IS USED
                float Uj[4];
                //Getting the Right Cell Solution Values
                cell *rightcell=rightcellface->neighbour;
                Uj[0]=rightcell->U[0];
                Uj[1]=rightcell->U[1];
                Uj[2]=rightcell->U[2];
                Uj[3]=rightcell->U[3];

                //Position Vector of Midpoint of face wrt Centroid
                float midnx,midny;
                midnx=(rightcellface->snode->x+rightcellface->lnode->x)*0.5;
                midny=(rightcellface->snode->y+rightcellface->lnode->y)*0.5;

                float rRx,rRy;
                rRx=midnx-rightcell->cx;
                rRy=midny-rightcell->cy;

                //Slope Limiter
                float Psi[4];
                for(int i=0;i<4;i++)
                    Psi[i]=rightcell->Psi[i];

                //Gradient*Position Vector
                float Prod[4];
                for(int iter=0;iter<4;iter++)
                {
                  Prod[iter]=rightcell->gradUx[iter]*rRx+rightcell->gradUy[iter]*rRy;
                }

                //RECONSTRUCTION
                for(int iter=0;iter<4;iter++)
                {
                  Ur[iter]=Uj[iter]+Psi[iter]*Prod[iter];
                }
            }
            else
            {
                cout<<endl<<rightcellface->next->neighbour->cellid<<"You screwed up..!"<<endl;
                exit(-5);
            }
        }
    }
}
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
//GIVES US UL BASED ON A RECONSTRUCTION
//choice VARIABLE IS THERE FOR SELECTION OF METHOD OF RECONSTRUCTION
//      1a- LINEAR RECONSTRUCTION BASED ON [BARTH,1989]
void leftstater_reconstructed(cell *leftcell,normal *currentface,float *Ul,char choice[2])
{
//cout<<endl<<"DEBUG : INSIDE LEFTSTATER";
  //Getting the left cell values in Ui
  float Ui[4];
  for(int i=0;i<4;i++)
  {
      Ui[i]=leftcell->U[i];
//cout<<endl<<"DEBUG : left cell values"<<Ui[i];
  }

//cout<<endl<<"DEBUG : LEFTCELL VALUES OBTAINED";

  //Position Vector of Midpoint of face wrt Centroid
  float midnx,midny;
  midnx=(currentface->snode->x+currentface->lnode->x)*0.5;
  midny=(currentface->snode->y+currentface->lnode->y)*0.5;
//cout<<endl<<"DEBUG :"<<a<<","<<b;

  float rlx,rly;
  rlx=midnx-leftcell->cx;
  rly=midny-leftcell->cy;
//cout<<endl<<"DEBUG : position vector"<<rlx<<","<<rly;
//cout<<endl<<"DEBUG : BEFORE LIMITER";
  //Slope Limiter
  float Psi[4];
  for(int iter=0;iter<4;iter++)
    Psi[iter]=leftcell->Psi[iter];
//cout<<endl<<"DEBUG : AFTER LIMITER";

  //Gradient*Position Vector
  float Prod[4];
  for(int iter=0;iter<4;iter++)
  {
    Prod[iter]=leftcell->gradUx[iter]*rlx+leftcell->gradUy[iter]*rly;
  }

  //RECONSTRUCTION
  for(int iter=0;iter<4;iter++)
  {
    Ul[iter]=Ui[iter]+Psi[iter]*Prod[iter];
  }
}
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// VENKATAKRISHNAN'S LIMITER HERE
void limiter_VKS(cell* currentcell)
{
//cout<<endl<<"DEBUG : INSIDE LIMITER"<<currentcell->cellid;
  ///////////////////////////////////////////////////////////////////////////////
  //TESTING
//  for(int i=0;i<4;i++)
//  {
//    currentcell->Psi[i]=1;
//  }
//  return;
  ///////////////////////////////////////////////////////////////////////////////

  //GETTING THE Umax and Umin
  ///////////////////////////////////////////////////////////////////////////////
  normal *currentface=currentcell->fnormal;
  float Umax[4],Umin[4],Ujdummy[4];
  for(int i=0;i<4;i++)
  {
    Umax[i]=currentcell->U[i];
    Umin[i]=currentcell->U[i];
  }
//cout<<endl<<"DEBUG : FIRST STEP IN LIMITER FUNCTION"<<currentcell->cellid;

  //Looping through all the current cell faces
  while(currentface!=NULL)
  {
    rightstater(currentcell,currentface,Ujdummy);
//cout<<endl<<"DEBUG : RIGHTSTATER IN LIMITER FUNCTION"<<currentcell->cellid;
    for(int i=0;i<4;i++)
    {
      if(Ujdummy[i]>Umax[i])
        Umax[i]=Ujdummy[i];
      if(Ujdummy[i]<Umin[i])
        Umin[i]=Ujdummy[i];
    }
    currentface=currentface->next;
  }
//cout<<endl<<"DEBUG : Umax,Umin FOUND"<<currentcell->cellid;
  //DELTA1MIN AND DELTA1MAX
  float delta1min[4],delta1max[4];
  for(int iter=0;iter<4;iter++)
  {
    delta1max[iter]=Umax[iter]-currentcell->U[iter];
    delta1min[iter]=Umin[iter]-currentcell->U[iter];
  }
//cout<<endl<<"DEBUG : DELTA1MIN,DELTA1MAX FOUND"<<currentcell->cellid;
  //FINDING DELTA2
  //////////////////////////////////////////////////////////////////////////////////
  float delta2[4],Prod[4];
  //Position Vector of Midpoint of face wrt Centroid
  float midnx,midny;
  currentface=currentcell->fnormal;
//cout<<endl<<"DEBUG : FACE NODES"<<currentface->snode->x<<","<<currentface->snode->y<<":"<<currentface->lnode->x<<","<<currentface->lnode->y;
  midnx=(currentface->snode->x+currentface->lnode->x)*0.5;
  midny=(currentface->snode->y+currentface->lnode->y)*0.5;
//cout<<endl<<"DEBUG : MIDN FOUND"<<currentcell->cellid;
  float rlx,rly;
  rlx=midnx-currentcell->cx;
  rly=midny-currentcell->cy;

//cout<<endl<<"DEBUG : POSITION VECTOR"<<rlx<<","<<rly;

  for(int iter=0;iter<4;iter++)
  {
//cout<<endl<<"DEBUG : GRADIENT OF U"<<currentcell->gradUx[iter]<<","<<currentcell->gradUy[iter];
    Prod[iter]=currentcell->gradUx[iter]*rlx+currentcell->gradUy[iter]*rly;
//cout<<endl<<"DEBUG : PROD CALCED"<<currentcell->cellid;
    if(Prod[iter]<0)
      delta2[iter]=(-1)*(abs(Prod[iter])+omega);
    else
      delta2[iter]=(1)*(abs(Prod[iter])+omega);
  }
//cout<<endl<<"DEBUG : DELTA2 FOUND"<<currentcell->cellid;
  // GETTING PSI
  ///////////////////////////////////////////////////////////////////////////////////////
  for(int i=0;i<4;i++)
  {
    if(delta2[i]>0)
    {
      float num=(delta1max[i]*delta1max[i]+pow(K,3)*currentcell->cvolsigned)*delta2[i]+2*delta2[i]*delta2[i]*delta1max[i];
      float den=(delta1max[i]*delta1max[i]+2*delta2[i]*delta2[i]+delta1max[i]*delta2[i]+pow(K,3)*currentcell->cvolsigned)*delta2[i];
      currentcell->Psi[i]=num/den;
    }
    else if(delta2[i]<0)
    {
      float num=(delta1min[i]*delta1min[i]+pow(K,3)*currentcell->cvolsigned)*delta2[i]+2*delta2[i]*delta2[i]*delta1min[i];
      float den=(delta1min[i]*delta1min[i]+2*delta2[i]*delta2[i]+delta1min[i]*delta2[i]+pow(K,3)*currentcell->cvolsigned)*delta2[i];
      currentcell->Psi[i]=num/den;
    }
    else
    {
      currentcell->Psi[i]=1;
    }
  }
  for(int i=0;i<4;i++)
  {
      currentcell->Psi[i]=0;
  }
}
// End of limiter_VKS function
/////////////////////////////////////////////////////////////////////////////////
#endif //_SOLVER_FUNCTIONS
//END OF THE HIGHER ORDER SOLVER FUNCTIONS
/////////////////////////////////////////////////////////////////////////////////
#endif //_Solver_h
/////////////////////////////////////////////////////////////////////////////////
