close all;clear;clc
format long e;
%%
X=importdata('Centroids_x.csv');
Y=importdata('Centroids_y.csv');
Solutionxvelocity=importdata('Solution_xvelocity.csv');
Solutionyvelocity=importdata('Solution_yvelocity.csv');
Solutiontemperature=importdata('Solution_temperature.csv');
Solutiondensity=importdata('Solution_density.csv');
%%
% X=X(2:end,:);
% Y=Y(2:end,:);
%%
Velocity=sqrt(Solutionxvelocity.^2+Solutionyvelocity.^2);
Mach=abs(Velocity)./sqrt(1.4*287.*Solutiontemperature);
Pressure=Solutiondensity.*Solutiontemperature*287;
%%
figure(4)
quiver(X,Y,Solutionxvelocity,Solutionyvelocity,0.5)
title('Velocity Vectors')
axis equal

figure(1)
h=pcolor(X,Y,abs(Mach));
set(h,'EdgeColor','none');
colormap(jet(5000));
colorbar();
title('Mach')
axis equal;

%%
figure(5)
contour(X,Y,abs(Mach),75);
colormap(jet(5000));
colorbar();
title('Mach')
axis equal;

figure(10)
contour(X,Y,Pressure,75);
colormap(jet(5000));
colorbar();
title('Pressure')
axis equal;


%%
figure(2)
h=pcolor(X,Y,(Solutiontemperature));
set(h,'EdgeColor','none');
colormap(hot(5000));
% contour(Solutiontemperature,75);
colorbar();
title('Temperature in Kelvin')
axis equal;

%%
figure(3)
h=pcolor(X,Y,(Pressure));
set(h,'EdgeColor','none');
colormap(jet(5000));
colorbar();
title('Pressure in Pascals')
axis equal; 
%%
figure(6)
h=pcolor(X,Y,Solutionxvelocity);
set(h,'EdgeColor','none');
colormap(jet(5000));
colorbar();
title('X component of velocity ms^-1')
axis equal;
%%
figure(7)
h=pcolor(X,Y,(Solutionyvelocity));
set(h,'EdgeColor','none');
colormap(jet(4096));
colorbar();
title('Y component of velocity ms^-1')
axis equal;

%% 
figure(14)
h=pcolor(X,Y,((Solutiondensity)));
set(h,'EdgeColor','none');
% contour(X,Y,Solutiondensity,75);
colormap(jet);
colorbar();
title('Density');
axis equal;
%% 
% Data=importdata('Data_Dump.txt');
% cellid=Data(:,1);
% x=Data(:,2);
% y=Data(:,3);
% density=Data(:,4);
% u=Data(:,5);
% v=Data(:,6);
% T=Data(:,7);
% a=Data(:,8);
% M=sqrt(u.*u+v.*v)./a;
% pointsize=5;
% figure(9)
% %scatter(atand(y./x),pointsize,density*287.*T,'filled')
% n=1;
% for i=1:length(x)
%     if(y(i)>(max(y)+min(y))*0.5)
%         solnpress(i-n+1,1)=x(i)-0.5*(max(x)+min(x));
%         solnmach(i-n+1,1)=x(i)-0.5*(max(x)+min(x));
%         solnpress(i-n+1,2)=((density(i)*287.*T(i)/(101325*6)));
%         solnmach(i-n+1,2)=(M(i));
%     else
%         n=n+1;
%     end
% end
% plot(solnpress(:,1),solnpress(:,2),'k.')
% hold on
% plot(solnmach(:,1),solnmach(:,2),'r.')
% hold on
% xlabel('x Coordinate')
% ylabel('CFD results')
% title('Variation over cylinder surface')
% legend('Pressure Ratio','Mach');
% colormap(jet(5000))
% colorbar();
% axis normal