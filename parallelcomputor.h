///MESHMAAMU
#ifndef _PARALLEL_COMPUTOR
#define _PARALLEL_COMPUTOR
//PUT IN THE ITERATIONS HERE..
//RETURN THE MAX DENSITY RESIDUAL..
//STEP AS MANY CELLS AS NUMBER OF CORES AVAILABLE
//RIGHT NOW HIGHLY SPECIFIC TO QUAD CORE..
//EXTEND TO MULTI CORE SYSTEMS
//MAYBE 4 THREADS NOT NEEDED.. PARENT THREAD CAN HANDLE 1/4TH THE JOB
//SO MABYE ONLY 3 EXTRA THREADS WOULD SUFFICE..
#define NUM_CORES 4
/////////////////////////////////////////////////////////////////////////////////
float iterator_maamu(cell*,cell**);
void* tstepper_cell(void *);
void starter_finder(cell*,cell**);
/////////////////////////////////////////////////////////////////////////////////
struct passer
{
    cell *starter;
    cell *ender;
};
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
void starter_finder(cell *firstc,cell **start)
{
    cell *start0=NULL,*start1=NULL,*start2=NULL,*start3=NULL,*start4=NULL;
    cell *currentc=firstc;
    int cellcount=0;
    while(currentc!=NULL)
    {
        cellcount++;
        currentc=currentc->next;
    }
    start0=firstc;
    currentc=firstc;
    int i=0;
    while(currentc!=NULL)
    {
        if(i==(int)(cellcount/4.0))
        {
            start1=currentc;
        }
        if(i==(int)(cellcount/2.0))
        {
            start2=currentc;
        }
        if(i==(int)(cellcount/1.33333333333))
        {
            start3=currentc;
        }
        i++;
        start4=currentc;
        currentc=currentc->next;
    }
    start[0]=start0;
    start[1]=start1;
    start[2]=start2;
    start[3]=start3;
    start[4]=start4;
}
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
float iterator_maamu(cell *firstc,cell **start)
{
    cell *currentc=firstc;
////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
    cell *start0,*start1,*start2,*start3,*start4;
//    int cellcount=0;
//    while(currentc!=NULL)
//    {
//        cellcount++;
//        currentc=currentc->next;
//    }
//    start0=firstc;
//    currentc=firstc;
//    int i=0;
//    while(currentc!=NULL)
//    {
//        if(i==(int)(cellcount/4.0))
//        {
//            start1=currentc;
//        }
//        if(i==(int)(cellcount/2.0))
//        {
//            start2=currentc;
//        }
//        if(i==(int)(cellcount/1.33333333333))
//        {
//            start3=currentc;
//        }
//        i++;
//        start4=currentc;
//        currentc=currentc->next;
//    }
//    cout<<endl<<start0->cellid;
//    cout<<endl<<start1->cellid;
//    cout<<endl<<start2->cellid;
//    cout<<endl<<start3->cellid;
////////////////////////////////////////////////////////////////////
    start0=start[0];
    start1=start[1];
    start2=start[2];
    start3=start[3];
    start4=start[4];
////////////////////////////////////////////////////////////////////
    pthread_t thread[4];
    pthread_attr_t attr;
    int rc;
    void *status;
////////////////////////////////////////////////////////////////////
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
////////////////////////////////////////////////////////////////////
    passer *pass0=new passer,*pass1=new passer,*pass2=new passer,*pass3=new passer;
    pass0->starter=start0;
    pass0->ender=start1;
    pass1->starter=start1;
    pass1->ender=start2;
    pass2->starter=start2;
    pass2->ender=start3;
    pass3->starter=start3;
    pass3->ender=start4->next;
////////////////////////////////////////////////////////////////////
//    pass0->ender=start2;
//    pass2->ender=start4->next;
////////////////////////////////////////////////////////////////////
    rc=pthread_create(&thread[0],&attr,tstepper_cell,(void *)pass0);
    rc=pthread_create(&thread[1],&attr,tstepper_cell,(void *)pass1);
    rc=pthread_create(&thread[2],&attr,tstepper_cell,(void *)pass2);
//    rc=pthread_create(&thread[3],&attr,tstepper_cell,(void *)pass3);
////////////////////////////////////////////////////////////////////
    currentc=start3;
    while(currentc!=start4->next)
    {
        if(currentc->cellid!=0)
        {
            ode45_tstep(currentc);
            currentc->scaled_density_residual=abs(currentc->Utstep[0]-currentc->U[0])/(currentc->U[0]);
        }
        currentc=currentc->next;
    }

////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
    pthread_attr_destroy(&attr);
    rc=pthread_join(thread[0],&status);
    rc=pthread_join(thread[1],&status);
    rc=pthread_join(thread[2],&status);
//    rc=pthread_join(thread[3],&status);
    if(rc)
    {
        ;
    }
////////////////////////////////////////////////////////////////////
    currentc=firstc;
    float densityresidual=-1;
    while(currentc!=NULL)
    {
        if(currentc->cellid!=0)
        {
            densityresidual=max(densityresidual,currentc->scaled_density_residual);
            for(int i=0;i<4;i++)
            {
                currentc->U[i]=currentc->Utstep[i];
            }
        }
        currentc=currentc->next;
    }
//    cout<<endl<<"Max density residual\t"<<densityresidual;
    return densityresidual;
}
/////////////////////////////////////////////////////////////////////////////////
void* tstepper_cell(void *passed_start_end_info)
{
    //cout<<"\nHello Maamu..!!\n";
    passer *passed_info=(passer*)passed_start_end_info;
//    cout<<endl<<currentc->cellid;
    cell *start=passed_info->starter;
    cell *last=passed_info->ender;
    cell *currentc=start;
    while(currentc!=last)
    {
        if(currentc->cellid!=0)
        {
            ode45_tstep(currentc);
            currentc->scaled_density_residual=abs(currentc->Utstep[0]-currentc->U[0])/(currentc->U[0]);
        }
        currentc=currentc->next;
    }
    pthread_exit((void*)passed_start_end_info);
    cout<<endl<<"\aComing here..!! Not supposed to..!!"<<endl;
    return NULL;
}
/////////////////////////////////////////////////////////////////////////////////
#endif // _PARALLEL_COMPUTOR
/////////////////////////////////////////////////////////////////////////////////
