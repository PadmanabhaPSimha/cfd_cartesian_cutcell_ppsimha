# README #
CFD using AUSM and AUSM+up schemes with explicit RK4 time stepping.
To get this code running, do not change the relative positions of any headers in the main folder.
A body file along with a domain definition file is a must to get started as no null case handler is written yet.
This is C++ 2011 compatible. No real OOP is done here. The classes are used as an efficient way of grouping 
related data and associated functions together.
To extract data from Data_Dump file, read the column vectors and then make an x-y grid.
Recommended post processing softwares are Matlab and/or TecPlot.
Matlab script for basic post processing included in the repository.


### What is this repository for? ###

CFD, AUSM, RK4 explicit, Body immersed in background mesh
If you encounter problems with nan or instabilities in temperatures/densities,
roll back to the last stable saved "Load-Dump".
Every 1000 iterations or so, keep copying the dump files and put them in a separate directory so 
that you keep a record of all successful solutions.
file that was created, reduce CFL number by about 30 percent and let it run again.

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

The files included are the main file and all associated headers, post processing Matlab script and some data files
pertaining to body geometry definitions, domain size and background discretisations and some output files for example.
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Padmanabha Prasanna Simha - Repository owner and primary code developer.
* Other community or team contact