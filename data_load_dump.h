///MESHMAAMU
#ifndef _data_load_dump_h
#define _data_load_dump_h
using namespace std;
//HERE YOU CAN WRITE A LINKED LIST TO FILE, READ IT FROM FILE
//WRITE ONLY THE NECESSARY SOLUTION VARIABLES AND THE CELLID
//WRITE THE CELL COUNTS Lx, Ly, dx, dy IN A DOMAIN_DEFINITION FILE
//READ THAT TOO AND MODIFY THE GLOBAL EXTERN QUANTITIES.
int cellsx=1000;
int cellsy=11;
long double Lx=10;
long double Ly=1;
int nodesx=cellsx+1;
int nodesy=cellsy+1;
long double dx=Lx/cellsx;
long double dy=Ly/cellsy;
/////////////////////////////////////////////////////////////////////////////////
void printer(cell*);
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
void dump_data(cell *firstc)
{
    fstream domain_dumper("Domain_definitor.txt",ios::out);
    domain_dumper<<cellsx<<"\t"<<cellsy<<"\t"<<Lx<<"\t"<<Ly;
    domain_dumper.close();
    fstream data_dumper("Load_Dump.txt",ios::out);
    cell *currentcell=firstc;
    while(currentcell!=NULL)
    {
        data_dumper<<currentcell->cellid<<"\t"<<currentcell->U[0]<<"\t"<<currentcell->U[1]<<"\t"<<currentcell->U[2]<<"\t"<<currentcell->U[3]<<"\t"<<currentcell->dt<<"\t"<<currentcell->a<<endl;
        currentcell=currentcell->next;
    }
    cout<<endl<<"Data dumped to file..!!"<<endl;
    data_dumper.close();
}
void print_dump(cell *firstc)
{
    fstream printing_dumper("Data_Dump.txt",ios::out);
    cell *currentcell=firstc;
    float u,v,T,a;
//    printing_dumper<<"\"CID\", \"X\", \"Y\", \"RHO\", \"U\", \"V\", \"T\", \"M\""<<endl;
    while(currentcell!=NULL)
    {
        if(currentcell->interior==-1)
        {
            u=currentcell->U[1]/currentcell->U[0];
            v=currentcell->U[2]/currentcell->U[0];
            T=(1/Cv)*((currentcell->U[3]/currentcell->U[0])-0.5*(u*u+v*v));
            a=sqrt(gamma*R*T);
            printing_dumper<<currentcell->cellid<<"\t"<<currentcell->cx<<"\t"<<currentcell->cy<<"\t"<<currentcell->U[0]<<"\t"<<u<<"\t"<<v<<"\t"<<T<<"\t"<<a<<endl;
        }
        currentcell=currentcell->next;
    }
    cout<<endl<<"Dumped flow variable data to file Data_Dump.txt \n\t\t\t- Paddymania Maamu"<<endl;
}
/////////////////////////////////////////////////////////////////////////////////////
void load_data(cell *firstc)
{
    fstream data_loader("Load_Dump.txt",ios::in);
    cell *currentcell=firstc;
    cout<<endl<<cellsx*cellsy<<endl;
    int i=1;
    long double cid,u0,u1,u2,u3,dtim,asound;
    while(currentcell!=NULL)
    {
        data_loader>>cid>>u0>>u1>>u2>>u3>>dtim>>asound;
        if(i%500==0)
        {
            std::cout<<std::endl<<i;
        }
        i++;
/////////////////////////////////////////////////////////////////////////////////////
        currentcell->U[0]=u0;
        currentcell->U[1]=u1;
        currentcell->U[2]=u2;
        currentcell->U[3]=u3;
/////////////////////////////////////////////////////////////////////////////////////
        for(int i=0;i<4;i++)
        {
            currentcell->Utstep[i]=currentcell->U[i];
        }
        currentcell=currentcell->next;
    }
    printer(firstc);
    cout<<"\nData initialization from file complete..!!\n";
    data_loader.close();
}
/////////////////////////////////////////////////////////////////////////////////////
void load_domain()
{
    fstream domain_loader("Domain_definitor.txt",ios::in);
    domain_loader>>cellsx>>cellsy>>Lx>>Ly;
    cout<<endl<<"Domain parameters - "<<endl<<dx<<endl<<dy<<endl<<Lx<<endl<<Ly<<endl<<endl<<endl;
    nodesx=cellsx+1;
    nodesy=cellsy+1;
    dx=Lx/cellsx;
    dy=Ly/cellsy;
    cout<<endl<<cellsx<<endl<<cellsy<<endl;
    cout<<endl<<"Domain definition read from file..!!"<<endl;
    domain_loader.close();
}
/////////////////////////////////////////////////////////////////////////////////////
//void surfacedump(cell *firstc)
//{
//    fstream surfacedata("Surfacedata.txt",ios::out);
//    cell *currentc=firstc;
//    float u,v,a,T;
//    while(currentc!=NULL)
//    {
//        if(currentc->interior==-1)
//        {
//            u=currentcell->U[1]/currentcell->U[0];
//            v=currentcell->U[2]/currentcell->U[0];
//            T=(1/Cv)*((currentcell->U[3]/currentcell->U[0])-0.5*(u*u+v*v));
//            a=sqrt(gamma*R*T);
//            surfacedata<<currentcell->cellid<<"\t"<<currentcell->cx<<"\t"<<currentcell->cy<<"\t"<<currentcell->U[0]<<"\t"<<u<<"\t"<<v<<"\t"<<T<<"\t"<<a<<endl;
//        }
//        currentcell=currentcell->next;
//    }
//    cout<<endl<<"Dumping body surface data";
//}
/////////////////////////////////////////////////////////////////////////////////////
#endif // _data_load_dump_h
/////////////////////////////////////////////////////////////////////////////////////
