///MESHMAAMU
//AS OF NOW CLOCKED AT 1.3 MICROSECONDS PER CELL PER ITERATION ON MY HP P073TX LAPPY
//CREATES A BLOCK OF NODES, BLOCK OF CELLS, SMALL RINGS OF NODES
//FEEDS THEM ALL SOME DATA AND CREATES THE QUAD BLOCK MESH
//KEEP GRID FINE. EACH ITERATION IS SUPERFAST..
//TRY TO IMPLEMENT A BETTER FLUX SPLITTING OR FLUX DIFFERENCE SPLITTING ALGO
/////////////////////////////////////////////////////////////////////////////////
///MAKE THIS A MIX QUADTREE/LINKED LIST IMPLEMENTATION
/////////////////////////////////////////////////////////////////////////////////
#ifndef _constants_gas
#define _constants_gas
#define R 287.0
#define gamma 1.4
#define Cv 717.5
#endif // _constants_gas
/////////////////////////////////////////////////////////////////////////////////
#ifndef CFL
#define CFL 0.1500
#define CONVERGENCE 1e-6
#define DIVERGENCE 1e+2
#endif //CFL
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
//THESE ARE IN DATA_LOAD_DUMP.H GET SHARED BETWEEN SEVERAL HEADERS
//THEY GET SET IN DIFFERENT HEADERS
//THEY GET MODIFIED IN DIFFERENT HEADERS
extern long double dx;
extern long double dy;
extern long double Lx;
extern long double Ly;
/////////////////////////////////////////////////////////////////////////////////
//DO NOT MODIFY THE ORDER OF THE HEADER FILE CALLS
//IF YOU DO SO, YOU MIGHT MESS UP THE DEPENDENCIES
//THOUGH I HAVE TRIED TO KEEP THEM QUITE INDEPENDENT BUT STILL SOME EXIST
//OR DO SOME MAGIC AND SOME KINDS OF FORWARD DECLARATIONS
//TO CORRECT ANY DEPENDENCY ERRORS IF THEY COME
//BUT DON'T MESS IT UP
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <pthread.h>
#include "cell.h"
#include "boundaries.h"
#include "Solver.h"
#include "cutcell.h"
//#include "refinery.h"
#include "body_disposal.h"
#include "data_load_dump.h"
#include "parallelcomputor.h"
/////////////////////////////////////////////////////////////////////////////////
#define ITER 100000
#define DISPLAY_ITER 10
#define PRINT_ITER 100
#define DUMP_ITER 1000
/////////////////////////////////////////////////////////////////////////////////
#ifndef _PARALLEL_COMPUTOR
#define _SERIAL_COMPUTOR
#endif // _PARALLEL_COMPUTOR
/////////////////////////////////////////////////////////////////////////////////
///THESE VARIABLES COME FROM data_load_dump.h HEADER FILE
extern int nodesx;
extern int nodesy;
extern int cellsx;//=nodesx-1;
extern int cellsy;//=nodesy-1;
/////////////////////////////////////////////////////////////////////////////////
using namespace std;
/////////////////////////////////////////////////////////////////////////////////
///THIS FUNCTION IS USED FOR THE SETTING UP OF THE INITIAL BACKGROUND MESH
node* local_node_quad(int,node*);
///THIS PRINTS RESULTS TO FILE.. USABLE IF CELL REFINEMENT/MERGING IS NOT TAKEN UP
void printer(cell*);
/////////////////////////////////////////////////////////////////////////////////
int main()
{
    ///COUT SOME BASIC MEMORY REQUIREMENTS OF EACH CONTAINER
    cout<<"\nSize of numerical storage qty in bytes - "<<sizeof(float)<<endl;
    cout<<"\nSize of each cell in bytes - "<<sizeof(cell)<<endl;
    cout<<"\nSize of each normal in bytes - "<<sizeof(normal)<<endl;
    cout<<"\nSize of each node in bytes - "<<sizeof(node)<<endl;
    //PROMPT FOR LOADING INITIALIZED SOLUTION FROM FILE
    //SET A FLAG
    //IF FLAG IS SET, INITIALIZE DATA FROM PREVIOUS FILE
    //ELSE DO THE THING THE NORMAL WAY
    char ans;
    bool init_data_available=false;
    if(ITER>1500)
    {
        ///DEFAULT ANSWER TO BE GIVEN BY USER IS A NO FOR A NEW TEST CASE
        cout<<"\nDo you have initialized data for the bodies you have set?(y/n):";
        cin>>ans;
        if(ans=='y')
        {
            init_data_available=true;
            ///MAYBE PUT SOMETHING IN THIS WHICH CHECKS IF YOU REALLY HAVE THE STUFF
            load_domain();
        }
        else
        {
            init_data_available=false;
        }
    }
/////////////////////////////////////////////////////////////////////////////////
//READ BODY DATA FROM FILE
    int body_no[_number_of_bodies];
    for(int i=0;i<_number_of_bodies;i++)
    {
        body_no[i]=bodysize(body[i]);
    }
    node *bodynodes[_number_of_bodies];
    for(int i=0;i<_number_of_bodies;i++)
    {
        bodynodes[i]=new node[body_no[i]];
    }
    for(int i=0;i<_number_of_bodies;i++)
    {
        cout<<endl<<"I'm here loading body "<<i+1<<" "<<body[i]<<endl;
        cout<<endl<<"Body size of "<<i+1<<" = "<<body_no[i];
        loadbody(bodynodes[i],body_no[i],body[i]);
        cout<<endl<<"Successfully loaded body "<<i+1;
    }
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
    //CREATE A QUAD BLOCK OF NODES NODESX ALONG X AND NODESY ALONG Y
    //BOTTOM LEFT IS AT GLOBAL ORIGIN
    cout<<"\nCreating nodes\nThis stuff will be done in no time at all..!!\nSo fasten your seat-belts..!!\n";
    node *firstn,*currentn,*newn,*lastn;
    firstn=new node;
    firstn->x=0;
    firstn->y=0;
    firstn->gnodeid=1;
    firstn->next=NULL;
    currentn=firstn;
/////////////////////////////////////////////////////////////////////////////////
    for(int i=2;i<=nodesx*nodesy;i++)
    {
        newn=new node;
        newn->x=((i-1)%nodesx)*dx;
        newn->y=(int)((i-1)/nodesx)*dy;
        newn->gnodeid=i;
        currentn->next=newn;
        newn->next=NULL;
        currentn=newn;
    }
    currentn=firstn;
    while(currentn!=NULL)
    {
        currentn=currentn->next;
    }
    lastn=currentn;
/////////////////////////////////////////////////////////////////////////////////
    //WRITE NODES TO FILE
    fstream nodesout("Nodedata.csv",ios::out);
    currentn=firstn;
    for(int i=1;i<=nodesy;i++)
    {
        for(int j=1;j<=nodesx;j++)
        {
            nodesout<<"("<<currentn->gnodeid<<"|"<<currentn->x<<"|"<<currentn->y<<")"<<",";
            currentn=currentn->next;
        }
        nodesout<<endl;
    }
/////////////////////////////////////////////////////////////////////////////////
//CREATE QUAD BLOCK OF CELLS HERE AND PUT IN SMALL LINKED LISTS OF 4 NODES EACH
    currentn=firstn;
    cell *firstc,*currentc,*newc,*lastc;
    node *tempn,*creepyn;
/////////////////////////////////////////////////////////////////////////////////
    int neighs[]={-1,2,cellsx+1,-2};
/////////////////////////////////////////////////////////////////////////////////
    tempn=local_node_quad(1,firstn);
    firstc=new cell(1,tempn,neighs);
    currentc=firstc;
    currentc->next=NULL;
    creepyn=firstn;
/////////////////////////////////////////////////////////////////////////////////
    cout<<"\nCreating cells\n"<<"It's going to take longer than you think..!!\n";
    cout<<"\nWell, maybe not anymore..!!\n Paddymania on the loose..!!\n";
    for(int i=2;i<=cellsx*cellsy;i++)
    {
        if(i%cellsx!=0)
        {
            //replace all creepyn by tempn in case things muck up...
            if(i>=nodesx)
            {
                creepyn=getnode(i+(int)i/cellsx-cellsx,creepyn);
            }
            else
            {
                creepyn=firstn;
            }
            tempn=local_node_quad(i+(int)i/cellsx,creepyn);
/////////////////////////////////////////////////////////////////////////////////
            neighs[0]=i-cellsx;
            neighs[1]=i+1;
            neighs[2]=i+cellsx;
            neighs[3]=i-1;
            //GLOBAL DOMAIN BOUNDARIES..
            //WALLS AT TOP AND BOTTOM
            //INFLOW AT LEFT
            //OUTFLOW AT RIGHT
//////////////////////////////////////////////////////////////////
            //WALL AT BOTTOM
            if(i<=cellsx)
            {
                neighs[0]=-1;
            }
            //WALL AT TOP
            if(i>cellsx*cellsy-cellsx)
            {
                neighs[2]=-1;
            }
            //INFLOW -2
            if(i%cellsx==1)
            {
                neighs[3]=-1;
            }
            //OUTFLOW -3
            if(i%cellsx==0)
            {
                neighs[1]=-1;
            }
        }
        else
        {
            tempn=local_node_quad(i+(int)i/cellsx-1,creepyn);
/////////////////////////////////////////////////////////////////////////////////
            neighs[0]=i-cellsx;
            neighs[1]=i+1;
            neighs[2]=i+cellsx;
            neighs[3]=i-1;
            //GLOBAL DOMAIN BOUNDARIES..
            //WALLS AT TOP AND BOTTOM
            //INFLOW AT LEFT
            //OUTFLOW AT RIGHT
            if(i<=cellsx)
            {
                neighs[0]=-1;
            }
            if(i>cellsx*cellsy-cellsx)
            {
                neighs[2]=-1;
            }
            if(i%cellsx==1)
            {
                neighs[3]=-1;
            }
            if(i%cellsx==0)
            {
                neighs[1]=-1;
            }
        }
/////////////////////////////////////////////////////////////////////////////////
        //put according to inflow or wall or whatever
        firstc->fnormal->next->next->next->neighbourid=-2;
        firstc->fnormal->neighbourid=-1;
/////////////////////////////////////////////////////////////////////////////////
        newc=new cell(i,tempn,neighs);
        currentc->next=newc;
        currentc=currentc->next;
        currentc->next=NULL;
    }
    lastc=currentc;
    cout<<endl<<"Background mesh cell count\t"<<cellsx*cellsy<<endl;
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
//PRE PROCESSING FOR NEIGHBOURS
    currentc=firstc;
    cout<<endl<<"\"Pre processing\" the grid \n(Processing before the processing of the \"to be processed\" grid)"<<endl;
    cout<<"You better grab something to eat.. \nThis stuff is gonna take a while..!!!"<<endl;
    cout<<"\nWell.. Not any more apparently,\n PPSimha rules the dark side of the force..!!!"<<endl;
    cout<<"His command over the force reduced this time\n to almost nothing compared to before..!!"<<endl;
    //LOOP THROUGH ALL THE CELLS TO SET THEIR NEIGHBOURS
    //CREEPER LIKE LINKED LIST. YOU HAVE CELLS ATTACHED TO
    //THEIR SURROUNDING 4 CELLS
    //THE BACKBONE OF THIS DATASTRUCTURE IS THE CELLS LINEAR LINKED LIST
    //EACH CELL HAS FACES WITH ASSOCIATED NORMAL CLASS OBJECTS
    //THE NORMAL CLASS OBJECT HAS A NEIGHBOUR POINTER POINTING TO THE CELL
    //INTO WHICH THE NORMAL IS POINTING INTO
    cell *formerc=firstc;
    normal *temporary_normal_main;
//    cell *nuller=new cell;
    while(currentc!=NULL)
    {
        //START AT FIRST NORMAL
        temporary_normal_main=currentc->fnormal;
        while(temporary_normal_main!=NULL)
        {
            //CHECK IF NOT BOUNDARY
            if(temporary_normal_main->neighbourid>0)
            {
                temporary_normal_main->neighbour=getcell(temporary_normal_main->neighbourid,formerc->fnormal->neighbour);
            }
            //IF BOUNDARY CELL, ASSIGN FIRSTCELL TO THE NEIGHBOUR AS ACCESS SHOULDN'T BE DONE
            else
            {
                temporary_normal_main->neighbour=firstc;
            }
            temporary_normal_main=temporary_normal_main->next;
        }
        formerc=currentc;
        currentc=currentc->next;
    }
/////////////////////////////////////////////////////////////////////////////////
    ///put according to inflow or wall or whatever
    firstc->fnormal->neighbourid=-1;
    firstc->fnormal->next->next->next->neighbourid=-2;
    firstc->fnormal->next->next->neighbourid=cellsx+1;
    firstc->fnormal->next->neighbourid=2;
    //firstc->cellid=0;
/////////////////////////////////////////////////////////////////////////////////
    //START SOLVER ITERATIONS HERE
    //SOLVER LOOPS THROUGH LINKED LIST OF CELLS
    //SUMMING UP FLUXES ACROSS ALL FACES AND COMPUTES RESIDUALS
    //RESIDUALS ARE USED IN AN RK4 ROUTINE
    //RK4 ROUTINE TAKES IN THE LEFT STATE AND UPDATES IT
/////////////////////////////////////////////////////////////////////////////////
//INITIALIZE THE CELL DATA HERE
//IF FLAG IS NOT SET
//IF FLAG IS SET, INITIALIZE FROM DATA FILE
    currentc=firstc;
    if(!init_data_available)
    {
        while(currentc!=NULL)
        {
            currentc->U[0]=1*P_inlet/(R*1*T_inlet);
            currentc->U[1]=0*currentc->U[0]*U_inlet;
            currentc->U[2]=currentc->U[0]*V_inlet;
            currentc->U[3]=currentc->U[0]*(Cv*1*T_inlet+0.5*(0*U_inlet*U_inlet+V_inlet*V_inlet));
            if(currentc->cx>5)
            {
                currentc->U[0]=0.1*P_inlet/(R*1*T_inlet);
                currentc->U[1]=0*currentc->U[0]*U_inlet;
                currentc->U[2]=0;
                currentc->U[3]=currentc->U[0]*(Cv*1*T_inlet+0.5*(0*U_inlet*U_inlet+V_inlet*V_inlet));
            }


//            if(currentc->cx>0.75&&currentc->cy<0.70)
//            {
//                currentc->U[2]=-0.9*currentc->U[1];
//                currentc->U[1]=0;
//                currentc->U[3]=currentc->U[0]*(Cv*T_inlet+0.5*(0.81*U_inlet*U_inlet+V_inlet*V_inlet));
//            }
//            else if(currentc->cx>0.75&&currentc->cy>=0.70)
//            {
//                currentc->U[2]=-0.70711*currentc->U[1];
//                currentc->U[1]=-0.70711*currentc->U[2];
//            }
            for(int i=0;i<4;i++)
            {
                currentc->Utstep[i]=currentc->U[i];
            }
            currentc->interior=0;
            currentc=currentc->next;
        }
    }
    //"DELETE" BODY INTERIOR HERE
    //DELETION DONE HERE AS THIS NEEDS NEIGHBOUR POINTERS TO DO
    cout<<"\nI'm here deleting the body(ies)\n";
    cout<<"Order of the number of body points and the cell count product\n";
    cout<<"SO.. This may take time..!!"<<endl;
//PARALLELIZE THIS.. CAN BE DONE EMBARASSINGLY PARALLELY
//THIS I THINK IS THE LONGEST STEP OF THE MESHING AND INITIALIZING PART
//MAYBE EVEN THE ADAPTIVE MESHER SHOULD BE PARALLELY DONE WHEN MADE
    for(int i=0;i<_number_of_bodies;i++)
    {
        cout<<endl<<"Deleting body interior and cutting body edges\t"<<i+1;
        cutbody(firstc,body_no[i],bodynodes[i]);
        cout<<endl<<"Deleted body interior and cut the cells"<<i+1;
    }
///MERGE SMALL CELLS TO AVOID CONVERGENCE ISSUES
///THIS PART MORE OR LESS WORKS WITH NO PROBLEMS..
///DO THIS ONLY AND ONLY AFTER ALL CELL REFINEMENT..
///IF REFINING CELLS AGAIN, DO NOT USE THE ORIGINAL NODE LIST
///REMOVE THE REPEATED NORMAL AND REMAKE THE NODE LIST
///CUT CELLS ONLY AFTER CELL REFINEMENT/COARSENING
///THEN AFTER CELLCUTTING WHICH IS DONE AFTER REFINEMENT/COARSENING,
///PERFORM THE MERGING...
///THIS IS THE FINAL PROCESSING STEP OF THE MESH.. TO IMPROVE SMOOTHNESS
#ifdef _REFINERY_H
    merge_smallcells_genbycut(firstc);
#endif //_REFINERY_H
//    }
    cout<<"\nWriting basic cell data to file after body deletion..!!\n";
    printer(firstc);
//PRINT CELL CENTROIDS TO FILE FOR VISUALISATION PURPOSES
    fstream node_coord_out_x("Centroids_x.csv",ios::out);
    fstream node_coord_out_y("Centroids_y.csv",ios::out);
    cout<<"\nWriting centroids to file..!!\n";
    currentc=firstc;
//    node *centroid;
    int i=1;
    while(currentc!=NULL)
    {
        if(currentc->cellid==0)
        {
            node_coord_out_x<<currentc->cx<<",";
            node_coord_out_y<<currentc->cy<<",";
        }
        else if(currentc->cellid!=0)
        {
            node_coord_out_x<<currentc->cx<<",";
            node_coord_out_y<<currentc->cy<<",";
        }
        if(i%cellsx==0)
        {
            node_coord_out_x<<endl;
            node_coord_out_y<<endl;
        }
        i++;
        currentc=currentc->next;
    }

//
//    while(currentc!=NULL)
//    {
//        centroid=currentc->Centroid();
//        node_coord_out_x<<currentc->cx<<",";
//        node_coord_out_y<<currentc->cy<<",";
//        delete centroid;
//        if(i%cellsx==0)
//        {
//            node_coord_out_x<<"\n";
//            node_coord_out_y<<"\n";
//        }
//        i++;
//        currentc=currentc->next;
//    }
    node_coord_out_x.close();
    node_coord_out_y.close();
    if(init_data_available)
    {
        cout<<endl<<"Initializing data\n";
        load_data(firstc);
    }
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
//ITERATIONS AND TIME STEPPING HERE
    cout<<"\nSolving..!\n";
    cout<<"CFL = "<<CFL<<endl;
    currentc=firstc;
/////////////////////////////////////////////////////////////////////////////////
#ifdef _PARALLEL_COMPUTOR
//CALL THE PARALLEL COMPUTING FUNCTION HERE TO TIME STEP THE CELLS INTO THE FUTURE..
//PASS THE RESIDUAL AS AN ARGUMENT..
    float densityresidualnorm;
    cell *start[5]={NULL,NULL,NULL,NULL,NULL};
    starter_finder(firstc,start);
    cout<<endl<<start[0]->cellid;
    cout<<endl<<start[1]->cellid;
    cout<<endl<<start[2]->cellid;
    cout<<endl<<start[3]->cellid;
    cout<<endl<<start[4]->cellid;
    float time=0;
    for(int i=1;i<=ITER;i++)
    {
        #ifdef _TIMESTEP
            time+=_TIMESTEP;
        #endif // _TIMESTEP
        densityresidualnorm=iterator_maamu(firstc,start);
        if(i%PRINT_ITER==0)
        {
            cout<<endl<<"Printing to file\n";
            print_dump(firstc);
//            surfacedump(firstc);
            printer(firstc);
        }
        if(i%DUMP_ITER==0)
        {
            cout<<endl<<"Dumping data to the dump if needed for future initialization\n";
            dump_data(firstc);
        }
        if(i%DISPLAY_ITER==0)
        {
            cout<<endl<<"Iteration#"<<i<<"\tDensity residual\t"<<densityresidualnorm;
        }
        if(densityresidualnorm<CONVERGENCE)
        {
            cout<<endl<<"Convergence achieved..!! Dumping data to file"<<endl;
            cout<<endl<<"\a";
            dump_data(firstc);
            break;
        }
        if(densityresidualnorm>DIVERGENCE)
        {
            cout<<endl<<"Divergence detected..!! Please take care of initial conditions..!!";
            cout<<endl<<"Please check your boundaries..!!"<<endl;
            cout<<endl<<"\a";
            exit(-1);
        }
    }
    currentc=firstc;
    cout<<endl<<endl<<"Denity residual norm = "<<densityresidualnorm<<endl<<endl;
    printer(firstc);
    #ifdef _TIMESTEP
        cout<<"Flow time = "<<time;
    #endif // _TIMESTEP
    ///DO I REALLY NEED TO FREE UP THE MEMORY?
    ///WHO CARES? DESTRUCTOR IS AUTO CALLED WHEN OBJS GO OUT OF SCOPE..!!
    pthread_exit(NULL);
#endif // _PARALLEL_COMPUTOR
/////////////////////////////////////////////////////////////////////////////////
#ifdef _SERIAL_COMPUTOR
    //TIME STEPPING DONE HERE.
    //UPDATE THE CONDITIONS FOR RESIDUAL BASED CONVERGENCE
    float densityresidualnorm;
    for(int i=1;i<=ITER;i++)
    {
        //int j=0;
        currentc=firstc;
        //LOOP THROUGH THE CELLS HERE
        while(currentc!=NULL)
        {
            //j++;
            //IF CURRENT CELL IS A BODY CELL, IGNORE IT
            //DON'T PERFORM THE 4 RK4 ITERATIONS EACH WITH 4 FLUX ITERATIONS
            if(currentc->cellid==0)
            {
                currentc=currentc->next;
                continue;
            }
            //cout<<endl<<"I'm getting in here..!!";
            //STEP THE CURRENT CELL IN TIME IF NOT INSIDE THE BODY
            ode45_tstep(currentc);
            //COMPUTE SOME SORT OF RESIDUAL
            currentc->scaled_density_residual=abs(currentc->Utstep[0]-currentc->U[0])/abs(currentc->U[0]);
            densityresidualnorm=currentc->scaled_density_residual;
            currentc=currentc->next;
        }
        //UPDATE THE U VECTOR USING UTSTEP VECTOR
        //THIS IS TO AVOID GAUSS SEIDEL LIKE BEHAVIOUR OF THE LIST
        //MAYBE GAUSS SEIDEL TYPE ITERATIONS WILL BE FASTER??
        currentc=firstc;
        densityresidualnorm=firstc->scaled_density_residual;
        while(currentc!=NULL)
        {
            for(int j=0;j<4;j++)
            {
                currentc->U[j]=currentc->Utstep[j];
            }
            if(currentc->cellid!=0)
            {
                densityresidualnorm=max(densityresidualnorm,currentc->scaled_density_residual);
            }
            currentc=currentc->next;
        }
        //cout<<endl<<j<<endl;
        //DISPLAY TO SCREEN
        if(i%DISPLAY_ITER==0)
        {
            cout<<endl<<"Iteration#"<<i<<"\tDensity residual\t"<<densityresidualnorm;
        }
        //WRITE DATA TO FILE FOR IMMEDIATE VIEWING
        if(i%PRINT_ITER==0)
        {
            cout<<endl<<"Printing to file\n";
            printer(firstc);
//            print_dump(firstc);
        }
        if(i%DUMP_ITER==0)
        {
            cout<<endl<<"Dumping data to the dump if needed for future initialization\n";
            dump_data(firstc);
        }
        if(densityresidualnorm<CONVERGENCE)
        {
            cout<<endl<<"Convergence achieved..!! Dumping data to file"<<endl;
            cout<<endl<<"\a";
            dump_data(firstc);
            break;
        }
        if(densityresidualnorm>DIVERGENCE)
        {
            cout<<endl<<"Divergence detected..!! Please take care of initial conditions..!!";
            cout<<endl<<"Please check your boundaries..!!"<<endl;
            cout<<endl<<"\a";
            exit(-1);
        }
    }
    cout<<endl<<"Iterations complete\t"<<endl;
    cout<<endl<<"Final density residual\t"<<densityresidualnorm<<endl;
//PRINT SOME RESULTS TO FILE
    printer(firstc);
#endif
/////////////////////////////////////////////////////////////////////////////////
    //DUMP FINAL DATA TO FILE AND USE FOR FUTURE INITIALIZATIONS
    delete lastn;
    delete lastc;
////////////////////////////
////////////////////////////
    pthread_exit(NULL);
//    return 0;
}
/////////////////////////////////////////////////////////////////////////////////
//THIS FUNCTION CREATES A SMALL QUAD LINKED LIST OF NODES
node* local_node_quad(int nid,node *firstnode)
{
    node *firstn,*currentn,*newn,*temp;
/////////////////////////////////////////////////////////////////////////////////
    firstn=new node;
    currentn=firstn;
    temp=getnode(nid,firstnode);
    firstn->gnodeid=nid;
//    firstn->nodeid=0;
    firstn->x=temp->x;
    firstn->y=temp->y;
/////////////////////////////////////////////////////////////////////////////////
    newn=new node;
    temp=getnode(nid+1,firstnode);
    currentn->next=newn;
    newn->gnodeid=nid+1;
//    newn->nodeid=1;
    newn->x=temp->x;
    newn->y=temp->y;
    currentn=newn;
/////////////////////////////////////////////////////////////////////////////////
    newn=new node;
    temp=getnode(nid+1+nodesx,firstnode);
    currentn->next=newn;
    newn->gnodeid=nid+1+nodesx;
//    newn->nodeid=2;
    newn->x=temp->x;
    newn->y=temp->y;
    currentn=newn;
/////////////////////////////////////////////////////////////////////////////////
    newn=new node;
    temp=getnode(nid+nodesx,firstnode);
    currentn->next=newn;
    newn->gnodeid=nid+nodesx;
//    newn->nodeid=3;
    newn->x=temp->x;
    newn->y=temp->y;
    currentn=newn;
/////////////////////////////////////////////////////////////////////////////////
    //COMPLETE CIRCULAR LIST
    newn->next=firstn;
/////////////////////////////////////////////////////////////////////////////////
    return firstn;
}

//THIS FUNCTION PRINTS EVERYTHING TO FILE
//IF CELLID=0 JUST GIVE A TAB
void printer(cell *firstc)
{
/////////////////////////////////////////////////////////////////////////////////
    //PRINT CELLS TO FILE
    cell *currentc;
    currentc=firstc;
    float tstep=firstc->dt;
    while(currentc!=NULL)
    {
        if(currentc->dt<tstep&&currentc->cellid!=0)
        {
            tstep=currentc->dt;
        }
        currentc=currentc->next;
    }
    cout<<"\nMinimum time step size.\nDon't trust this number..\nJust for show\t"<<tstep;
    fstream cellsout("Celldata.csv",ios::out);
    currentc=firstc;
    int i=1;
    while(currentc!=NULL)
    {
        if(currentc->cellid==0)
        {
            cellsout<<",";
        }
        else if(currentc->cellid!=0)
        {
            cellsout<<"("<<currentc->cellid<<"|"<<currentc->fnormal->next->next->neighbourid<<")"<<",";
        }
        if(i%cellsx==0)
        {
            cellsout<<endl;
        }
        i++;
        currentc=currentc->next;
    }
/////////////////////////////////////////////////////////////////////////////////
    //PRINT RESULTS TO FILE
    fstream resultsout1("Solution_xvelocity.csv",ios::out);
    currentc=firstc;
    i=1;
    while(currentc!=NULL)
    {
        if(currentc->cellid==0)
        {
            resultsout1<<",";
        }
        else
        {
            resultsout1<<currentc->U[1]/currentc->U[0]<<",";
        }
        if(i%cellsx==0)
        {
            resultsout1<<endl;
        }
        i++;
        currentc=currentc->next;
    }
    fstream resultsout2("Solution_yvelocity.csv",ios::out);
    currentc=firstc;
    i=1;
    while(currentc!=NULL)
    {
        if(currentc->cellid==0)
        {
            resultsout2<<",";
        }
        else
        {
            resultsout2<<currentc->U[2]/currentc->U[0]<<",";
        }
        if(i%cellsx==0)
        {
            resultsout2<<endl;
        }
        i++;
        currentc=currentc->next;
    }
    fstream resultsout3("Solution_density.csv",ios::out);
    currentc=firstc;
    i=1;
    while(currentc!=NULL)
    {
        if(currentc->cellid==0)
        {
            resultsout3<<",";
        }
        else
        {
            resultsout3<<currentc->U[0]<<",";
        }
        if(i%cellsx==0)
        {
            resultsout3<<endl;
        }
        i++;
        currentc=currentc->next;
    }
    fstream resultsout4("Solution_temperature.csv",ios::out);
    currentc=firstc;
    i=1;
    while(currentc!=NULL)
    {
        if(currentc->cellid==0)
        {
            resultsout4<<",";
        }
        else
        {
            resultsout4<<(1/Cv)*((currentc->U[3]/currentc->U[0])-0.5*(currentc->U[1]*currentc->U[1]+currentc->U[2]*currentc->U[2])/(currentc->U[0]*currentc->U[0]))<<",";
        }
        if(i%cellsx==0)
        {
            resultsout4<<endl;
        }
        i++;
        currentc=currentc->next;
    }
    cellsout.close();
    resultsout1.close();
    resultsout2.close();
    resultsout3.close();
    resultsout4.close();
    cout<<endl<<"Printing complete..!!"<<endl;
}
//END OF MAIN FUNCTION...!!!
//END OF MeshMaamu
